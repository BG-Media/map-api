<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CarsBrandsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->insert([
            'id' => 1,
            'name_ru' => 'Марки авто',
            'name_kk' => 'Маркалар',
            'name_en' => 'Marks',
        ]);

        DB::table('types')->insert([
            'id' => 2,
            'name_ru' => 'Модели авто',
            'name_kk' => 'Моделдер',
            'name_en' => 'Models',
        ]);
        DB::table('types')->insert([
            'id' => 3,
            'name_ru' => 'Тип транспорта',
            'name_kk' => 'Транспорт түрлері',
            'name_en' => 'Transport types',
        ]);


        DB::table('references')->insert([
            'id' => 1,
            'type_id' => 1,
            'name_ru' => 'Audi',
            'name_kk' => 'Audi',
            'name_en' => 'Audi',
            'parent_id' => 0
        ]);

        DB::table('references')->insert([
            'id' => 2,
            'type_id' => 1,
            'name_ru' => 'Nissan',
            'name_kk' => 'Nissan',
            'name_en' => 'Nissan',
            'parent_id' => 0
        ]);

        DB::table('references')->insert([
            'type_id' => 2,
            'name_ru' => 'A-56',
            'name_kk' => 'A-56',
            'name_en' => 'A-56',
            'parent_id' => 1
        ]);

        DB::table('references')->insert([
            'type_id' => 2,
            'name_ru' => 'A-99',
            'name_kk' => 'A-99',
            'name_en' => 'A-99',
            'parent_id' => 1
        ]);

        DB::table('references')->insert([
            'type_id' => 2,
            'name_ru' => 'N787',
            'name_kk' => 'N787',
            'name_en' => 'N787',
            'parent_id' => 2
        ]);

        DB::table('references')->insert([
            'type_id' => 2,
            'name_ru' => 'N-998',
            'name_kk' => 'N-123',
            'name_en' => 'N-89',
            'parent_id' => 2
        ]);

        /*Types*/

        DB::table('references')->insert([
            'type_id' => 3,
            'name_ru' => 'зерновоз',
            'name_kk' => 'зерновоз',
            'name_en' => 'зерновоз',
            'parent_id' => 0
        ]);

        DB::table('references')->insert([
            'type_id' => 3,
            'name_ru' => 'рефрижератор',
            'name_kk' => 'рефрижератор',
            'name_en' => 'рефрижератор',
            'parent_id' => 0
        ]);

        DB::table('references')->insert([
            'type_id' => 3,
            'name_ru' => 'битумовоз',
            'name_kk' => 'битумовоз',
            'name_en' => 'битумовоз',
            'parent_id' => 0
        ]);
    }
}