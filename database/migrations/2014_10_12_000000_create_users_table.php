<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->default('');
            $table->string('phone')->default('');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('role');
            $table->string('device_token')->unique();
            $table->string('avatar')->default('');
            $table->bigInteger('iin')->default(0);
            $table->string('actual_address')->default('');
            $table->string('legal_address')->default('');
            $table->integer('sphere_id')->default(0);
            $table->double('balance')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
