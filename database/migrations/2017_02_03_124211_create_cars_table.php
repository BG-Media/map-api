<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('brand');
            $table->integer('model');
            $table->integer('year');
            $table->integer('type');
            $table->double('volume');
            $table->double('capacity');
            $table->string('gov_number');
            $table->string('reg_place');
            $table->timestamp('date_tech_inspection');
            $table->string('insurance_policy');
            $table->text('description');
            $table->integer('status')->default(0);
            $table->double('time_history')->default(0);
            $table->double('distance_history')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}