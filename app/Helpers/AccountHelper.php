<?php

namespace App\Helpers;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Token;
use Illuminate\Support\Facades\Hash;
use Validator;
use Auth;

class AccountHelper
{
    public static function signToken($user)
    {
        return Token::create([
            'user_id' => $user['user_id'],
            'device' => $user['device_name'],
            'token' => $user['device_token'],
            'status' => 1
        ]);
    }

    public static function signUp($request)
    {
        return User::create([
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'device_token' => $request['device_token'],
            'role' => $request['role'],
        ]);
    }

    public static function signIn($request)
    {
        $user = User::where('email', $request['email'])
            ->where('role', $request['role'])
            ->first();

        if (!$user) {
            return [
                'status' => 'error',
                'result' => 'Пользователь с таким e-mail не найден'
            ];
        }

        if (!Hash::check($request['password'], $user->password)) {
            return [
                'status' => 'error',
                'result' => 'Пароль неверный'
            ];
        }

        if ($user->role == 'transporter') {
            if ($request['device_token'] == $user->device_token) {
                return [
                    'status' => 'success',
                    'result' => $user
                ];
            }
            else
            {
                if (Token::where('token', $request['device_token'])->count() == 0) {
                    Token::create([
                        'user_id' => $user->id,
                        'device' => $request['device_name'],
                        'token' => $request['device_token'],
                        'status' => 0
                    ]);
                }
                return [
                    'status' => 'error',
                    'result' => 'Вы зашли в нового устройства. Свяжитесь с администратором, для фиксации смены устройства'
                ];
            }
        }

        return [
            'status' => 'success',
            'result' => $user
        ];
    }
}