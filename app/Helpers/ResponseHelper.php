<?php

namespace App\Helpers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Transporter\OrdersController;
use App\Models\Media;
use App\Models\Order;
use App\Models\Reference;
use App\Models\User;
use App\Models\Token;
use Validator;
use Auth;

class ResponseHelper
{
    public static function showResponse($result, $status = 'success') {
        return response()->json([
            'status' => $status,
            'result' => $result
        ]);
    }

    public static function showErrors($errors) {
        return self::showResponse([
            'errors' => $errors
        ], 'error');
    }

    public static function showError($error) {
        return self::showResponse([
            'error' => $error
        ], 'error');
    }

    public static function showCars($cars, $full = false) {
        $list = [];

        foreach ($cars as $car)
        {
            $list[] = self::showCar($car, $full);
        }

        return self::showResponse($list, 'success');
    }

    public static function showCar($car, $full = false) {

        if ($full != 'usage')
        {
            $car->car = [
                'brand_name' => Reference::find($car->brand)->name_ru,
                'model_name' => Reference::find($car->model)->name_ru,
                'type' => Reference::find($car->type)->name_ru,
            ];

            $car->media = Media::where('entity', 'car')->where('entity_id', $car->id)->get();
        }
        $car->using = [
            'capacity' => Order::where('car_id', $car->id)->where('status', 1)->sum('capacity'),
            'volume' => Order::where('car_id', $car->id)->where('status', 1)->sum('volume'),
        ];
        $car->free = [
            'capacity' => $car->capacity - $car->using['capacity'],
            'volume' => $car->volume - $car->using['volume'],
        ];

        if ($full == 'full') {
            $current = Order::where('car_id', $car->id)->where('status', 1)->get();
            $history = Order::where('car_id', $car->id)->where('status', '>', 1)->get();
            $car->orders = [
                'current' => OrdersController::formatingOrders($current),
                'history' => OrdersController::formatingOrders($history),
            ];
        }

        return $car;
    }
}