<?php

namespace App\Providers;

use App\Models\Order;
use App\Models\Page;
use App\Models\Token;
use App\Models\Types;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $deviceChange = Token::where('status', 0)->count();
        View::share('deviceChange', $deviceChange);

        $findLocation = DB::table('locate')->where('moderated', 0)->count();
        View::share('findLocation', $findLocation);

        $types = Types::all();
        View::share('types', $types);


        $pages = Page::all();
        View::share('pages', $pages);

        $ordersCount = [
                -1 => Order::where('status', -1)->count(),
                0 => Order::where('status', 0)->count(),
                1 => Order::where('status', 1)->count(),
                2 => Order::where('status', 2)->count(),
                3 => Order::where('status', 3)->count(),
                4 => Order::where('status', 4)->count(),
            ];
        View::share('ordersCount', $ordersCount);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
