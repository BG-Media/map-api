<?php

namespace App\Http\Middleware;

use Closure;

class CheckWebToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = \App\Models\Token::where('token', $request->input('token'))->first();
        if (!$token)
        {
            return response()->json([
                'status' => 'error',
                'result' => [
                    'errors' => 'Web token mismatch'
                ]
            ]);
        }

        $user = \App\Models\User::find($token->user_id);

        if (!$user)
        {
            return response()->json([
                'status' => 'error',
                'result' => [
                    'errors' => 'Unknown user (id: '.$token->user_id.')'
                ]
            ]);
        }
        $request->safeUser = $user;
        return $next($request);


    }
}
