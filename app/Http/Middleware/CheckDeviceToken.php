<?php

namespace App\Http\Middleware;

use Closure;

class CheckDeviceToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = \App\Models\User::where('device_token', $request->input('device_token'))->first();
        if ($user)
        {
            if ($user->blockTime > time()) {
                return response()->json([
                    'status' => 'error',
                    'result' => [
                        'errors' => 'Вы заблокированы до '.date('d.m.Y, H:i', $user->blockTime)
                    ]
                ]);
            }

            $request->safeUser = $user;
            return $next($request);
        }

        return response()->json([
            'status' => 'error',
            'result' => [
                'errors' => 'Device token mismatch'
            ]
        ]);
    }
}
