<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Hash;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Hash::check(env('ADMIN_SECRETKEY'), $request->session()->get('secretKey'))) {
            return $next($request);
        }
        return redirect('/');
    }
}
