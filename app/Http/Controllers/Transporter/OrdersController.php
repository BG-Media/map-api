<?php

namespace App\Http\Controllers\Transporter;

use App\Models\Location;
use App\Models\Notification;
use App\Models\Order;
use App\Models\Car;
use App\Models\User;
use App\Models\Reference;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Validator;
use Dingo\Api\Routing\Helpers;
use Auth;
use App\Helpers\ResponseHelper;

class OrdersController extends Controller
{
    use Helpers;

    public function preLoad()
    {
        $result = [
            'locations' => Location::all(),
            'statuses' => [
                'Размещен',
                'Принят',
                'Отказ',
                'Доставлено',
                'Завершено',
            ],
            'insured' => Reference::where('type_id', 4)->select(['id', 'name_ru'])->get(),
            'typeTransp' => Reference::where('type_id', 5)->select(['id', 'name_ru'])->get(),
            'carTypes' => Reference::where('type_id', 3)->select(['id', 'name_ru'])->get(),
            'hazardClass' => Reference::where('type_id', 7)->select(['id', 'name_ru'])->get(),
        ];

        return ResponseHelper::showResponse($result);
    }

    public function getCount(Request $request, $return = 'count')
    {
        $query = false;

        if ($request->has('pointA')) {
            $query = Order::where('pointA', $request->input('pointA'));
        }

        if ($request->has('pointB')) {
            if ($query) {
                $query = $query->where('pointB', $request->input('pointB'));
            } else {
                $query = Order::where('pointB', $request->input('pointB'));
            }
        }

        if ($query) {

            if ($request->has('insured')) {
                $query = $query->where('insured', $request->input('insured'));
            }

            if ($request->has('typeTransp')) {
                $query = $query->where('typeTransp', $request->input('typeTransp'));
            }

            if ($request->has('carTypes')) {
                $query = $query->where('preferredCar', $request->input('carTypes'));
            }

            if ($request->has('hazardClass')) {
                $query = $query->where('hazardClass', $request->input('hazardClass'));
            }

            if ($request->has('capacity_min')) {
                $query = $query->where('capacity', '>=', $request->input('capacity_min'));
            }

            if ($request->has('capacity_max')) {
                $query = $query->where('capacity', '<=', $request->input('capacity_max'));
            }

            if ($request->has('volume_min')) {
                $query = $query->where('volume', '>=', $request->input('volume_min'));
            }

            if ($request->has('volume_max')) {
                $query = $query->where('volume', '<=', $request->input('volume_max'));
            }

            if ($request->has('price_min')) {
                $query = $query->where('price', '>=', $request->input('price_min'));
            }

            if ($request->has('price_max')) {
                $query = $query->where('price', '<=', $request->input('price_max'));
            }

            if ($return == 'count') {
                $result = ['total' => $query->where('status', 0)->count()];
            } else {
                $list = $query->where('status', 0)->orderBy('id', 'desc')->get();
                $result = $this->formatingOrders($list);
            }

            return ResponseHelper::showResponse($result);
        } else {
            return ResponseHelper::showError('Нужно указать хотя бы один адрес');
        }

    }

    public function getList(Request $request)
    {
        return $this->getCount($request, 'list');
    }

    public function myOrders(Request $request)
    {
        $list = Order::where('transporter_id', $request->safeUser->id)->orderBy('id', 'desc')->get();
        $orders = $this->formatingOrders($list);
        return ResponseHelper::showResponse($orders);
    }

    public function takeOrder(Request $request)
    {
        if (!$request->has('order_id')) {
            return ResponseHelper::showError('Нужно передеать `order_id`');
        }

        $order = Order::find($request->input('order_id'));

        if (!$order) {
            return ResponseHelper::showError('Груз не найден');
        }

        if ($order->status !== 0) {
            if ($order->transporter_id == $request->safeUser->id) {
                return ResponseHelper::showError('Вы уже взяли этот груз');
            } else {
                return ResponseHelper::showError('Груз принят другим перевозчиком');
            }
        }

        if ($request->safeUser->balance < 1) {
            return ResponseHelper::showError('Недостаточно средств на счету');
        }

        $car = Car::find($request->input('car_id'));
        $using_capacity = Order::where('car_id', $request->input('car_id'))->where('status', 1)->sum('capacity');

        if ($using_capacity > $car->capacity) {
            return ResponseHelper::showError('Вы не можете взять этот заказ. Объем превышен');
        }

        if ($order->capacity > $car->capacity) {
            return ResponseHelper::showError('Вы не можете взять этот заказ. Объем больше чем в машине');
        }

        $using_volume = Order::where('car_id', $request->input('car_id'))->where('status', 1)->sum('volume');
        if ($using_volume > $car->volume) {
            return ResponseHelper::showError('Вы не можете взять этот заказ. Вес превышен');
        }

        if ($order->volume > $car->volume) {
            return ResponseHelper::showError('Вы не можете взять этот заказ. Вес больше чем в машине');
        }


        $order->status = 1;
        $order->transporter_id = $request->safeUser->id;
        $order->car_id = $request->input('car_id');
        $order->save();

        return ResponseHelper::showResponse('Груз принят');
    }

    public function latest(Request $request)
    {
        $list = Order::where('status', 0);

        if ($request->has('search')) {
            $list = $list->where('description', 'like', '%' . $request->input('search') . '%');
        }

        $list = $list->orderBy('id', 'desc')->take(10)->get();

        $orders = $this->formatingOrders($list);

        return ResponseHelper::showResponse($orders);
    }

    public function delivered(Request $request)
    {
        if (!$request->has('order_id')) {
            return ResponseHelper::showError('Нужно передеать `order_id`');
        }

        $order = Order::find($request->input('order_id'));

        if (!$order) {
            return ResponseHelper::showError('Груз не найден');
        }

        if ($order->status === 3) {
            if ($order->transporter_id == $request->safeUser->id) {
                return ResponseHelper::showError('Груз уже доставлен, и ждет ободрения заказчика');
            } else {
                return ResponseHelper::showError('Груз принят другим перевозчиком');
            }
        }

        $order->status = 3;
        $order->save();

        return ResponseHelper::showResponse('Груз принят');
    }

    public function sendLocation(Request $request)
    {
        $notif = Notification::find($request->input('notification_id'));
        if ($notif->disabled == 0) {

            $update = [
                'response' => 1,
                'latitude' => $request->input('latitude'),
                'longitude' => $request->input('longitude')
            ];

            DB::table('locate')->
            where('id', $request->input('entity_id'))->
            update($update);

            $notif->disabled = 1;
            $notif->save();

            return ResponseHelper::showResponse($update);
        }
        return ResponseHelper::showError('Заблокировано');
    }

    public function find(Request $request)
    {
        $order = Order::find($request->input('order_id'));
        if ($order) {
            $order = $this->formatingOrder($order);
            return ResponseHelper::showResponse($order);
        } else {
            return ResponseHelper::showError('Заказ не найден');
        }
    }

    public static function formatingOrders($list)
    {
        $orders = [];

        foreach ($list as $item) {
            $item = self::formatingOrder($item);
            $orders[] = $item;
        }

        return $orders;
    }

    public static function formatingOrder($item) {
        $item->location = [
            'from' => Location::where('map_id', $item->pointA)->first(),
            'to' => Location::where('map_id', $item->pointB)->first(),
            'distance' => $item->meters
        ];
        $item->provisionalDate = date('d/m/y', $item->provisionalDate);
        $item->preferredCar = Reference::where('id', $item->preferredCar)->first()->name_ru;
        $item->car = Car::find($item->car_id);
        $item->insured = Reference::where('id', $item->insured)->first()->name_ru;
        $item->typeTransp = Reference::where('id', $item->typeTransp)->first()->name_ru;
        $item->hazardClass = Reference::where('id', $item->hazardClass)->first()->name_ru;
        $item->termsPayment = Reference::where('id', $item->termsPayment)->first()->name_ru;
        $item->loadingOption = Reference::where('id', $item->loadingOption)->first()->name_ru;

        $item->customer = User::find($item->customer_id);


        unset($item->pointA, $item->pointB, $item->meters, $item->car_id, $item->transporter_id);
        return $item;
    }
}
