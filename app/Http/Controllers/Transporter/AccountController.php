<?php

namespace App\Http\Controllers\Transporter;

use App\Models\Notification;
use App\Models\Reference;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Helpers\AccountHelper;
use App\Helpers\ResponseHelper;
use Illuminate\Support\Facades\Hash;

class AccountController extends Controller
{
    public function signUp(Request $request)
    {
        $rules = [
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'device_token' => 'required|unique:users'
        ];

        $messages = [
            'email.required' => 'E-mail обязателен',
            'email.email'  => 'E-mail не соответствует формату',
            'email.unique'  => 'E-mail уже занят',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return ResponseHelper::showErrors($validator->errors()->all());
        }

        $request['role'] = 'transporter';

        $user = AccountHelper::signUp($request);

        if ($user) {
            //dd($user->id);
            $request['user_id'] = $user->id;
            AccountHelper::signToken($request);
            return ResponseHelper::showResponse($user, 'success');
        }

        return ResponseHelper::showError('Database error! Try again!');
    }

    public function signIn(Request $request)
    {
        $rules = [
            'email' => 'required|email',
            'password' => 'required',
            'device_token' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return ResponseHelper::showErrors($validator->errors()->all());
        }

        $request['role'] = 'transporter';

        $result = AccountHelper::signIn($request);

        return ResponseHelper::showResponse($result['result'], $result['status']);
    }

    public function resetPassword(Request $request) {
        $user = User::where('email', $request->input('email'))->first();
        if (!$user) {
            return ResponseHelper::showError('Пользователь с таким email не найден');
        }

        if ($user->role == 'customer') {
            return ResponseHelper::showError('Пользователь должен быть перевозчиком');
        }

        $newPassword = substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789') , 0 , 8 );
        $user->password = bcrypt($newPassword);
        $user->save();

        $subject = 'Восстановление пароля';
        $message = 'Ваш новый пароль: '.$newPassword.' . Не забудьте сменить пароль в настройках';
        $headers = 'From: admin@mapplus.kz' . "\r\n" .
            'Reply-To: admin@mapplus.kz' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

        mail($request->input('email'), $subject, $message, $headers);

        return ResponseHelper::showResponse('Пароль создан и отправлен на почту '.$request->input('email'));
    }

		

    public function fetch(Request $request)
    {
        $request->safeUser->spheres = Reference::where('type_id', 11)->get();
        return ResponseHelper::showResponse($request->safeUser, 'success');
    }

    public function updateProfile(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            //'email' => 'email|unique:users,email,'.$request->safeUser->id,
            'phone' => 'unique:users,phone,' . $request->safeUser->id,
            'iin' => 'numeric|unique:users,iin,' . $request->safeUser->id,
            'sphere_id' => 'numeric'
        ]);
		

        if ($validator->fails()) {
            return ResponseHelper::showErrors($validator->errors()->all());
        }
		
		if ($request->hasFile('avatar')) {
            $path = $request->avatar->store($request->safeUser->id, 'users');
            $input['avatar'] = '/uploads/users/'.$path;
        }

        // @todo fix this shit
        unset($input['device_token'], $input['email'], $input['password'], $input['role'], $input['balance']);


        $request->safeUser->update($input);

        return ResponseHelper::showResponse($request->safeUser, 'success');
    }
	
	public function setPassword(Request $request) {
        $rules = [
            'oldPassword' => 'required',
            'newPassword' => 'required|different:oldPassword'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return ResponseHelper::showErrors($validator->errors()->all());
        }


        if (!Hash::check($request->input('oldPassword'), $request->safeUser->password)) {
            return ResponseHelper::showError('Старый пароль введен неправильно');
        }

        $request->safeUser->password = bcrypt($request['newPassword']);
        $request->safeUser->update();

        return ResponseHelper::showResponse([
            'result' => 'Успешно'
        ]);
    }

    public function notifications(Request $request) {
        $notifs = Notification::where('user_id', $request->safeUser->id)->orderBy('id', 'desc')->get();
        return ResponseHelper::showResponse($notifs);
    }
}