<?php

namespace App\Http\Controllers\Transporter;

use App\Models\Car;
use App\Models\Media;
use App\Models\Reference;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Dingo\Api\Routing\Helpers;
use Auth;
use App\Helpers\ResponseHelper;

class CarsController extends Controller
{
    use Helpers;

    public function addCar(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'brand' => 'required|integer',
            'model' => 'required|integer',
            'year' => 'required|integer',
            'type' => 'required',
            'volume' => 'required',
            'capacity' => 'required',
            'gov_number' => 'required',
            'reg_place' => 'required',
            'date_tech_inspection' => 'required',
            'insurance_policy' => 'required',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return ResponseHelper::showErrors($validator->errors()->all());
        }

        /**
         * Cleaning input var
         */
        unset($input['device_token']);
        $input['user_id'] = $request->safeUser->id;


        $car = Car::create($input);
        return ResponseHelper::showResponse($car, 'success');
    }

    public function listOptions()
    {
        $types = Reference::getCarTypes();
        $brands = Reference::getCarBrands();
        $list = [];

        foreach ($brands as $brand)
        {
            $brand->models = Reference::getCarModels($brand->id);
            $list[] = $brand;
        }


        return ResponseHelper::showResponse([
            'cars' => $list,
            'car_types' => $types,
        ], 'success');
    }

    public function myCars(Request $request)
    {
        $cars = Car::where('user_id', $request->safeUser->id)->get();

        return ResponseHelper::showCars($cars, 'full');
    }

    public function myCarsUsage(Request $request)
    {
        $cars = Car::where('user_id', $request->safeUser->id)->get(['id', 'user_id']);

        return ResponseHelper::showCars($cars, 'usage');
    }

    public function fetchCar(Request $request)
    {
        $car = Car::where('user_id', $request->safeUser->id)
            ->where('id', $request->input('car_id'))
            ->first();

        if ($car)
        {
            return ResponseHelper::showCar($car, 'full');
        }
        return ResponseHelper::showError('Машина не найдена');

    }

    public function addImage(Request $request)
    {
        $car = Car::where('user_id', $request->safeUser->id)
            ->where('id', $request->input('car_id'))
            ->first();

        if ($car)
        {
            if ($request->hasFile('image'))
            {
                $path = $request->image->store($car->id, 'cars');
                Media::create([
                    'entity' => 'car',
                    'entity_id' => $car->id,
                    'path' => '/uploads/cars/'.$path,
                ]);

                return ResponseHelper::showResponse(ResponseHelper::showCar($car, 'full'));
            }
            return ResponseHelper::showError('Изображение не найдено');

        }
        return ResponseHelper::showError('Машина не найдена');
    }
}
