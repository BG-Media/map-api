<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Models\News;
use App\Models\Page;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function about(Request $request) {
        $page = Page::find(1);
        $page->name = $page->{'name_'.$request->input('lang')};
        $page->content = $page->{'content_'.$request->input('lang')};
        return ResponseHelper::showResponse($page);
    }

    public function help(Request $request) {
        $page = Page::find(2);
        $page->name = $page->{'name_'.$request->input('lang')};
        $page->content = $page->{'content_'.$request->input('lang')};
        return ResponseHelper::showResponse($page);
    }

    public function map() {
        return view('map');
    }
}
