<?php

namespace App\Http\Controllers\Admin;

use App\Models\Customer;
use App\Models\Notification;
use App\Models\Order;
use App\Models\Transporter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public $statuses = [
        -1 => 'На модерации',
        'Размещен',
        'Принят',
        'Отказ',
        'Доставлено',
        'Завершено',
    ];

    public function index()
    {
        //
    }

    public function punish($id) {
        $order = Order::find($id);
        return view('dashboard.orders.punish', compact('order'));
    }

    public function punishing(Request $request, $id) {
        $order = Order::find($id);
        $order->status = 0;
        $order->save();

        $order->transporter->balance = $request->input('balance');
        $order->transporter->save();
        $message = 'Вы отказались от заказа №'.$id.'. Баланс равняется '.$request->input('balance');
        Notification::create([
            'user_id' => $order->transporter->id,
            'time' => time(),
            'entity_id' => $id,
            'type' => 'cancelOrder',
            'text' => $message,
        ]);
        $order->transporter->notify($message);

        return redirect()->route('admin.orders.byStatus', ['status' => 2]);
    }

    public function moderate($order) {
        $order = Order::find($order);
        $order->status = 0;
        $order->save();
        $users = Transporter::init()->get();
        $message = 'Новый заказ №'.$order->id.'. Из '.$order->locationA->label_ru .' в '.$order->locationB->label_ru.'
Оплата '.$order->price.'тг.';
        foreach ($users as $user) {
            $user->notify($message, $order->id);
            Notification::create([
                'user_id' => $user->id,
                'time' => time(),
                'entity_id' => $order->id,
                'type' => 'newOrder',
                'text' => $message,
            ]);
        }
        return redirect()->back();
    }

    public function byStatus($status = 0) {
        $orders = Order::where('status', $status)->get();
        $status = $this->statuses[$status];
        return view('dashboard.orders.byStatus', compact('orders', 'status'));
    }

    public function byCustomer($id) {
        $customer = Customer::find($id);
        $orders = Order::where('customer_id', $id)->get();
        return view('dashboard.orders.byCustomer', [
            'customer' => $customer,
            'orders' => $orders,
            'statuses' => $this->statuses
        ]);
    }

    public function byTransporter($id) {
        $transporter = Transporter::find($id);
        $orders = Order::where('transporter_id', $id)->get();
        return view('dashboard.orders.byTransporter', [
            'transporter' => $transporter,
            'orders' => $orders,
            'statuses' => $this->statuses
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
