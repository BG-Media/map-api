<?php

namespace App\Http\Controllers\Admin;

use App\Models\Notification;
use App\Models\Order;
use App\Models\Token;
use App\Models\Transporter;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use DB;
class TransporterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $searchMode = $request->has('search') ? $request->input('search') : false;
        if ($searchMode) {
            $users = DB::table('users')
                ->where('role', '=', 'transporter')
                ->where(function($query) use ($searchMode)
                {
                    $query->where('phone', $searchMode)
                        ->orWhere('iin', $searchMode);
                })
                ->get();
        } else {
            $users = Transporter::init()->get();
        }
        return view('dashboard.transporter.index', compact('users', 'searchMode'));
    }

    public function block($user) {
        $user = User::find($user);
        return view('dashboard.transporter.block', compact('user'));
    }

    public function blockInit($user, Request $request) {
        $user = Transporter::find($user);
        switch($request->input('unit')) {
            default:
            case 'hour':
                $label = 'ч.';
                $factor = 3600;
                break;

            case 'day':
                $label = 'дн.';
                $factor = 86400;
                break;

            case 'week':
                $label = 'нед.';
                $factor = 604800;
                break;

            case 'month':
                $label = 'мес.';
                $factor = 262800;
                break;

            case 'year':
                $label = 'г.';
                $factor = 31536000;
                break;
        }

        $blockTime = $request->input('number') * $factor;
        $message = 'Вы были заблокированы на '.$request->input('number').' '.$label;
        $user->blockTime = time() + $blockTime;

        $user->save();
        $user->notify($message);

        Notification::create([
            'user_id' => $user->id,
            'time' => time(),
            'entity_id' => 0,
            'type' => 'text',
            'text' => $message,
        ]);
        return redirect()->back();
    }

    public function findLocation() {
        $items = DB::table('locate')->where('moderated', 0)->get();
        $list = [];
        foreach($items as $item) {
            $item->order = Order::find($item->order_id);
            $list[] = $item;
        }
        return view('dashboard.transporter.findLocation', compact('list'));
    }

    public function approveLocation($id) {
        $locate = DB::table('locate')->find($id);
        DB::table('locate')->where('id', $id)->update(['moderated' => 1]);
        $order = Order::find($locate->order_id);
        Notification::create([
            'user_id' => $order->transporter_id,
            'time' => time(),
            'entity_id' => $id,
            'type' => 'findLocation',
            'text' => 'Заказчик запрашивает местоположение груза №'.$order->id,
        ]);
        $user = Transporter::find($order->transporter_id);
        $user->notify('Заказчик запрашивает местоположение груза №'.$order->id, $order->id);
        return redirect()->back();
    }

    public function deleteLocation($id) {
        DB::table('locate')->where('id', $id)->delete();
        return redirect()->back();
    }

    public function deviceChange() {
        $list = Token::where('status', 0)->get();
        return view('dashboard.transporter.deviceChange', compact('list'));
    }

    public function approveDevice($token_id) {
        $token = Token::find($token_id);
        $token->status = 1;
        $token->save();

        Token::where('token', $token->user->device_token)->delete();

        $token->user->device_token = $token->token;
        $token->user->save();

        return redirect()->back();
    }

    public function deleteDevice($token_id) {
        $token = Token::find($token_id);
        $token->delete();
        return redirect()->back();
    }

    public function formBalance() {
        return view('dashboard.transporter.formBalance');
    }

    public function updateBalance(Request $request) {
        $errors = $users = [];

        if ($request->hasFile('dump')) {

            $file = $request->dump->store('excel', 'temp');
            $path = public_path().'/uploads/temp/'.$file;
            $data = Excel::load($path, function($reader) {
            })->get();

            if (!empty($data) && $data->count()) {
                foreach ($data as $key => $value) {
                    $user = User::find($value->id);
                    if (!$user) {
                        $errors[] = 'Пользователь с ID '.$value->id .' не найден';
                    }
                    else if ($value->balance == null || $value->balance < 1) {
                        $errors[] = 'Баланс указан неверно ('.$value->balance.') для пользователя с ID '.$value->id;
                    }
                    else {
                        $user->balance+=$value->balance;
                        $user->update();
                        $user->newBalance = $value->balance;
                        $users[] = $user;
                    }
                }
            }

            unlink($path);

        }
        return view('dashboard.transporter.updateBalance', compact('users', 'errors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Transporter $user)
    {
        return view('dashboard.transporter.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Transporter $user)
    {
        return view('dashboard.transporter.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transporter $user)
    {
        $input = $request->all();
        $user->update($input);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transporter $user)
    {
		Token::where('user_id', $user->id)->delete();
        $user->delete();
        return redirect()->route('admin.transporter.index');
    }
}
