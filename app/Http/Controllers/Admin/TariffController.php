<?php

namespace App\Http\Controllers\Admin;

use App\Models\Currency;
use App\Models\Reference;
use App\Models\Tariff;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class TariffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tariffs = Tariff::all();
        $currencyList = DB::table('currency')->orderBy('id', 'desc')->get();
        return view('dashboard.tariff.list', compact('tariffs', 'currencyList'));
    }

    public function addMoney() {
        return view('dashboard.currency.add');
    }


    public function storeMoney(Request $request) {
        $this->validate($request, [
            'label' => 'required',
            'tenge' => 'required|numeric',
        ]);

        DB::table('currency')->insert([
           'label' => $request->input('label'),
           'tenge' => $request->input('tenge'),
        ]);

        return redirect()->route('admin.tariff.index');
    }


    public function editMoney($id) {
        $money = DB::table('currency')->find($id);
        return view('dashboard.currency.edit', compact('money'));
    }

    public function updateMoney($id, Request $request) {
        $this->validate($request, [
            'label' => 'required',
            'tenge' => 'required|numeric',
        ]);

        DB::table('currency')->
            where('id', $id)->
            update([
                'label' => $request->input('label'),
                'tenge' => $request->input('tenge'),
            ]);

        return redirect()->route('admin.tariff.index');
    }

    public function showMoney($id) {
        $money = DB::table('currency')->find($id);
        return view('dashboard.currency.show', compact('money'));
    }

    public function destroyMoney($id) {
        DB::table('currency')->where('id', $id)->delete();
        return redirect()->route('admin.tariff.index');
    }


    public function add() {
        $currencyList = Currency::orderBy('id', 'desc')->get()->unique('label');
        $carTypes = Reference::getCarTypes();
        return view('dashboard.tariff.add', compact('currencyList', 'carTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $input = $request->all();
        $input['start_date'] = $input['start_day'].'.'.$input['start_month'];
        $input['end_date'] = $input['end_day'].'.'.$input['end_month'];

        // cleaning

        unset($input['start_day'], $input['start_month'], $input['end_day'], $input['end_month']);

        Tariff::create($input);
        return redirect()->route('admin.tariff.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tariff = Tariff::find($id);
        return view('dashboard.tariff.show', compact('tariff'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tariff = Tariff::find($id);
        $currencyList = Currency::orderBy('id', 'desc')->get()->unique('label');
        $carTypes = Reference::getCarTypes();
        list($tariff->start_day, $tariff->start_month) = explode('.', $tariff->start_date);
        list($tariff->end_day, $tariff->end_month) = explode('.', $tariff->end_date);
        return view('dashboard.tariff.edit', compact('currencyList', 'carTypes', 'tariff'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tariff = Tariff::find($id);
        $input = $request->all();
        $input['start_date'] = $input['start_day'].'.'.$input['start_month'];
        $input['end_date'] = $input['end_day'].'.'.$input['end_month'];

        // cleaning

        unset($input['start_day'], $input['start_month'], $input['end_day'], $input['end_month']);

        $tariff->update($input);
        return redirect()->route('admin.tariff.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Tariff::find($id)->delete();
        return redirect()->route('admin.tariff.index');
    }
}
