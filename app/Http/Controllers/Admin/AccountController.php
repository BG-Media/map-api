<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AccountController extends Controller
{
    public function auth(Request $request) {
        if ($request->input('secretKey') == env('ADMIN_SECRETKEY')) {
            $request->session()->set('secretKey', bcrypt($request->input('secretKey')));
            return redirect()->route('adminIndex');
        }

        return redirect()->back();
    }

    public function digest() {
        return view('dashboard.index.digest');
    }
}
