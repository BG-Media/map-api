<?php

namespace App\Http\Controllers\Admin;

use App\Models\Types;
use App\Models\Reference;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TermsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $type_id = $request->has('type') ? $request->input('type') : 1;
        $type = Types::find($type_id);
        $refs = Reference::where('type_id', $type->id)->get();
        return view('dashboard.terms.list', compact('refs', 'type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $type = Types::find($request->input('type'));
        return view('dashboard.terms.create', compact('type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $ref = Reference::create($input);
        return redirect()->route('admin.terms.edit', ['type' => $ref->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ref = Reference::find($id);
        return view('dashboard.terms.show', compact('ref'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ref = Reference::find($id);
        $models = Reference::getCarBrands();
        return view('dashboard.terms.edit', compact('ref', 'models'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $ref = Reference::find($id);
        $ref->update($input);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ref = Reference::find($id);
        $ref->delete();
        return redirect()->route('admin.terms.index');
    }
}
