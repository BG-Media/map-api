<?php

namespace App\Http\Controllers\Customer;

use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Models\Ads;
use Validator;

class AdsController extends Controller
{
    public function listing() {
        return ResponseHelper::showResponse(Ads::all());
    }
}