<?php

namespace App\Http\Controllers\Customer;

use App\Helpers\ResponseHelper;
use App\Models\Reference;
use App\Models\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Helpers\AccountHelper;
use Illuminate\Support\Facades\Hash;

class AccountController extends Controller
{
    public function signUp(Request $request)
    {
        $rules = [
            'email' => 'required|email|unique:users',
            'password' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return ResponseHelper::showErrors($validator->errors()->all());
        }
        $request['role'] = 'customer';
        $request['device_token'] = str_random(60);
        $request['device_name'] = $_SERVER['HTTP_USER_AGENT'];

        $user = AccountHelper::signUp($request);

        if ($user) {
            //$user = User::find($user->id);
            $request['user_id'] = $user->id;
            AccountHelper::signToken($request);
            return ResponseHelper::showResponse($user, 'success');
        }
        return ResponseHelper::showError('Database error! Try again!');
    }

    public function signIn(Request $request)
    {
        $rules = [
            'email' => 'required|email',
            'password' => 'required'
        ];

        $request['role'] = 'customer';

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return ResponseHelper::showErrors($validator->errors()->all());
        }
        $result = AccountHelper::signIn($request);

        return ResponseHelper::showResponse($result['result'], $result['status']);
    }

    public function fetch(Request $request)
    {
        $request->safeUser->tokenFixed = date('d.m.Y H:i:s');
        $spheres = Reference::where('type_id', 11)->get();
        $items = [];
        foreach ($spheres as $sphere) {
            $sphere->name = $sphere->{'name_'.$request->input('lang')};
            $items[] = $sphere;
        }
        $request->safeUser->spheres = $items;
        return ResponseHelper::showResponse($request->safeUser);
    }

    public function updateProfile(Request $request)
    {
        $input = $request->all();
        $user_id = $request->safeUser->id;
        $validator = Validator::make($input, [
            'phone' => 'unique:users,phone,' . $user_id,
            'iin' => 'numeric',
            'sphere_id' => 'numeric'
        ]);

        if ($validator->fails()) {
            return ResponseHelper::showErrors($validator->errors()->all());
        }

        if ($request->hasFile('avatar')) {
            $path = $request->avatar->store($user_id, 'users');
            $input['avatar'] = '/uploads/users/'.$path;
        }

        // @todo fix this shit
        unset($input['device_token'], $input['email'], $input['password'], $input['role'], $input['balance']);

        $request->safeUser->update($input);
        return ResponseHelper::showResponse($request->safeUser, 'success');
    }

    public function setPassword(Request $request) {
        $rules = [
            'oldPassword' => 'required',
            'newPassword' => 'required|different:oldPassword'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return ResponseHelper::showErrors($validator->errors()->all());
        }


        if (!Hash::check($request->input('oldPassword'), $request->safeUser->password)) {
            return ResponseHelper::showError('Старый пароль введен неправильно');
        }

        $request->safeUser->password = bcrypt($request['newPassword']);
        $request->safeUser->update();

        return ResponseHelper::showResponse([
            'result' => 'Успешно'
        ]);
    }

    public function resetPassword(Request $request) {
        $user = User::where('email', $request->input('email'))->first();
        if (!$user) {
            return ResponseHelper::showError('Пользователь с таким email не найден');
        }

        if ($user->role == 'transporter') {
            return ResponseHelper::showError('Пользователь должен быть заказчиком');
        }

        $newPassword = substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789') , 0 , 8 );
        $user->password = bcrypt($newPassword);
        $user->save();

        $subject = 'Восстановление пароля';
        $message = 'Ваш новый пароль: '.$newPassword.' . Не забудьте сменить пароль в настройках';
        $headers = 'From: admin@mapplus.kz' . "\r\n" .
            'Reply-To: admin@mapplus.kz' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

        mail($request->input('email'), $subject, $message, $headers);

        return ResponseHelper::showResponse('Пароль создан и отправлен на почту '.$request->input('email'));
    }
}