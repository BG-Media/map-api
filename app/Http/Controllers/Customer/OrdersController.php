<?php

namespace App\Http\Controllers\Customer;

use App\Helpers\ResponseHelper;
use App\Models\Car;
use App\Models\Currency;
use App\Models\Media;
use App\Models\Order;
use App\Models\Reference;
use App\Models\Tariff;
use App\Models\Types;
use App\Models\Location;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class OrdersController extends Controller
{
    public function references(Request $request)
    {
        $types = Types::all();
        $result = [];

        foreach ($types as $type) {
            $refs = Reference::where('type_id', $type->id)->get();
            $list = [];
            foreach ($refs as $ref) {
                $ref->name = $ref->{'name_'.$request->input('lang')};
                $list[] = $ref;
            }
            $result[$type->label] = $list;
        }

        return ResponseHelper::showResponse($result);
    }

    public function tariff() {
        /**
         * Ugly code for find tariff
         */

        $currentDay = date('j');
        $currentMonth = date('n');
        $currentYear = date('Y');
        $tmpStr = $currentDay.'.'.$currentMonth;
        $tmpTimestamp = strtotime($tmpStr);

        $tariffs = Tariff::all();

        foreach ($tariffs as $tariff) {
            $startTimestamp = strtotime($tariff->start_date.'.'.$currentYear);
            $endTimestamp = strtotime($tariff->end_date.'.'.$currentYear);

            if ($tmpTimestamp > $startTimestamp && $tmpTimestamp < $endTimestamp) {
                return ResponseHelper::showResponse($tariff);
            }
        }

        $tariff = Tariff::first();
        return ResponseHelper::showResponse($tariff);
    }

    public function findLocation($id) {
        DB::table('locate')->insert([
            'order_id' => $id,
            'time' => time(),
        ]);
    }

    public function myOrders(Request $request)
    {
        $list = Order::where('customer_id', $request->safeUser->id)->orderBy('id', 'desc')->get();
        $orders = [];

        $fix_kk = $request->input('lang') == 'kk' ? 'kz' : $request->input('lang');

        foreach ($list as $item) {
            $item->from = Location::where('map_id', $item->pointA)->first();
            $item->from->label = $item->from->{'label_'.$fix_kk};
            $item->to = Location::where('map_id', $item->pointB)->first();
            $item->to->label = $item->to->{'label_'.$fix_kk};

            if ($item->transporter_id) {
                $item->transporter = User::find($item->transporter_id);
            }

            $item->provisionalDateFormated = date('d/m/y', $item->provisionalDate);
            $item->preferredCarFormated = Reference::where('id', $item->preferredCar)->first();
            $item->preferredCarFormated->name = $item->preferredCarFormated->{'name_'.$request->input('lang')};
            $orders[] = $item;
        }

        return ResponseHelper::showResponse($orders);
    }

    public function fetch($id, Request $request) {
        $fix_kk = $request->input('lang') == 'kk' ? 'kz' : $request->input('lang');

        $item = Order::where('customer_id', $request->safeUser->id)->where('id', $id)->first();
        $item->capacity = number_format($item->capacity, 3);
        $item->volume = number_format($item->volume, 3);

        $item->from = Location::where('map_id', $item->pointA)->first();
        $item->from->label = $item->from->{'label_'.$fix_kk};
        $item->to = Location::where('map_id', $item->pointB)->first();
        $item->to->label = $item->to->{'label_'.$fix_kk};

        $item->provisionalDateFormated = date('d/m/y', $item->provisionalDate);
        $item->preferredCarFormated = Reference::where('id', $item->preferredCar)->first();
        $item->preferredCarFormated->name = $item->preferredCarFormated->{'name_'.$request->input('lang')};
        $item->typeTranspFormated = Reference::where('id', $item->typeTransp)->first();
        $item->typeTranspFormated->name = $item->typeTranspFormated->{'name_'.$request->input('lang')};
        $item->hazardClassFormated = Reference::where('id', $item->hazardClass)->first();
        $item->hazardClassFormated->name = $item->hazardClassFormated->{'name_'.$request->input('lang')};
        $currencyList = Currency::orderBy('id', 'desc')->get()->unique('label');
        $tmp = [];

        foreach($currencyList as $money) {
            if ($money->label == 'KZT') {
                continue;
            }
            $tmp[] = [
                'price' => round($item->price / $money->tenge, 1),
                'label' => $money->label,
                'tenge' => $money->tenge,
            ];
        }

        $item->currency = $tmp;

        if ($item->transporter_id) {

            $item->transporter = User::where('id', $item->transporter_id)->select(['name', 'avatar', 'phone', 'actual_address', 'email'])->first();
            $item->transporter->avatar = 'http://'.$_SERVER['HTTP_HOST'].'/'.$item->transporter->avatar;
            $item->car = Car::find($item->car_id);
            if ($item->car_id) {
                $item->car->model = Reference::find($item->car->model);
                $item->car->brand = Reference::find($item->car->brand);
                $item->car->image = 'http://'.$_SERVER['HTTP_HOST'].'/'.Media::where('entity', 'car')->where('entity_id', $item->car_id)->first()->path;
                $date = Carbon::parse($item->car->date_tech_inspection);
                $item->car->date_tech_inspection_formated = $date->day.'/'.$date->month.'/'.$date->year;
                $item->car->using = [
                    'capacity' => Order::where('car_id', $item->car->id)->where('status', 1)->sum('capacity'),
                    'volume' => Order::where('car_id', $item->car->id)->where('status', 1)->sum('volume'),
                ];
                $item->car->free = [
                    'capacity' => $item->car->capacity - $item->car->using['capacity'],
                    'volume' => $item->car->volume - $item->car->using['volume'],
                ];
            }
        }

        $item->findLocation = DB::table('locate')->where('order_id', $item->id)->where('response', 1)->orderBy('id', 'desc')->first();
        if ($item->findLocation) {
            $item->findLocation->time = date('d.m.Y H:i', $item->findLocation->time);
        }


        return ResponseHelper::showResponse($item);
    }

    public function locationExists($point) {
        return Location::where('map_id', $point->map_id)->count() == 1;
    }

    public function addLocation($point) {
        $location = new Location;
        $location->map_id = $point->map_id;
        $location->coords = $point->coords;
        $location->label_ru = $point->label_ru;
        $location->label_en = $point->label_en;
        $location->label_kz = $point->label_kz;
        $location->address_ru = $point->address_ru;
        $location->address_en = $point->address_en;
        $location->address_kz = $point->address_kz;
        return $location->save();
    }

    public function addOrder(Request $request)
    {
        $input = $request->all();
        $from = json_decode($input['points']['from']);
        $to = json_decode($input['points']['to']);


        if ($this->locationExists($from) == false) {
            $this->addLocation($from);
        }

        if ($this->locationExists($to) == false) {
            $this->addLocation($to);
        }


        $order = Order::create([
            'pointA' => $from->map_id,
            'pointB' => $to->map_id,
            'meters' => $input['citiesLength'],
            'capacity' => $input['capacity'],
            'volume' => $input['volume'],
            'insured' => $input['insured'],
            'description' => $input['description'],
            'typeTransp' => $input['typeTransp'],
            'preferredCar' => $input['preferredCar'],
            'hazardClass' => $input['hazardClass'],
            'termsPayment' => $input['termsPayment'],
            'loadingOption' => $input['loadingOption'],
            'provisionalDate' => $input['provisionalDate'],
            'customer_id' => $request->safeUser->id,
            'price' => $input['price'],
            'status' => -1,
        ]);

        return ResponseHelper::showResponse($order);
    }

    public function complete(Request $request) {
        if (!$request->has('order_id')) {
            return ResponseHelper::showError('Нужно передеать `order_id`');
        }

        $order = Order::find($request->input('order_id'));

        if (!$order) {
            return ResponseHelper::showError('Груз не найден');
        }

        if ($order->customer_id == $request->safeUser->id) {
            if ($order->status === 4) {

                return ResponseHelper::showError('Груз уже завершен');
            }
        } else {
           return ResponseHelper::showError('Груз принадлежит другому заказчику');
        }

        $order->status = 4;
        $order->save();

        return ResponseHelper::showResponse('Груз завершен');
    }
}