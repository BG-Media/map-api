<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseHelper;
use App\Models\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function all(Request $request) {
        $news_tmp = News::all();
        $news = [];
        foreach ($news_tmp as $item) {
            $item->content = strip_tags($item->{'content_'.$request->input('lang')});
            $item->title = $item->{'title_'.$request->input('lang')};
            $news[] = $item;
        }
        return ResponseHelper::showResponse($news);
    }

    public function fetch($news, Request $request) {
        $item = News::find($news);
        $item->title = $item->{'title_'.$request->input('lang')};
        $item->content = strip_tags($item->{'content_'.$request->input('lang')});
        return ResponseHelper::showResponse($item);
    }
}
