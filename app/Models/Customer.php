<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public $table = 'users';
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'role',
        'avatar',
        'iin',
        'actual_address',
        'legal_address',
        'sphere_id',
        'device_token',
        'balance'
    ];

    protected $hidden = [
        'password',
        'updated_at',
        'created_at'
    ];

    public static function init()
    {
        return self::where('role', 'customer');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Order', 'customer_id', 'id');
    }
}
