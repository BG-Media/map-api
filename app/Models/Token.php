<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    protected $fillable = ['user_id','device','token','status'];

    public function user() {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
}