<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Tariff extends Model
{
    protected $guarded = ['id'];

    public $table = 'tariff';
    public $timestamps = false;
}