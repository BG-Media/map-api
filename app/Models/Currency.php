<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Currency extends Model
{
    protected $guarded = ['id'];

    public $table = 'currency';
    public $timestamps = false;
}