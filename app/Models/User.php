<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'role',
        'avatar',
        'iin',
        'actual_address',
        'legal_address',
        'sphere_id',
        'device_token',
        'balance',
        'device_os',
        'push_token',
    ];

    protected $hidden = [
        'password',
        'updated_at',
        'created_at'
    ];

}
