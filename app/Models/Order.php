<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Order extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;

    public function locationA() {
        return $this->hasOne('App\Models\Location', 'map_id', 'pointA');
    }

    public function locationB() {
        return $this->hasOne('App\Models\Location', 'map_id', 'pointB');
    }

    public function customer() {
        return $this->hasOne('App\Models\User', 'id', 'customer_id');
    }

    public function transporter() {
        return $this->hasOne('App\Models\Transporter', 'id', 'transporter_id');
    }
}