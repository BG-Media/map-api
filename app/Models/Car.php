<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    public $table = 'cars';

    public $guarded = ['id'];
    public $hidden = ['updated_at', 'created_at'];
}
