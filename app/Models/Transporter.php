<?php

namespace App\Models;

use PushNotification;
use Illuminate\Database\Eloquent\Model;

class Transporter extends Model
{
    public $table = 'users';
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'role',
        'avatar',
        'iin',
        'actual_address',
        'legal_address',
        'sphere_id',
        'device_token',
        'balance',
        'security_code',
        'device_os',
        'push_token',
    ];

    protected $hidden = [
        'password',
        'updated_at',
        'created_at'
    ];

    public static function init()
    {
        return self::where('role', 'transporter');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Order', 'transporter_id', 'id');
    }

    public function notify($message, $id = 0) {
        if (!empty($this->push_token)) {
            $os = $this->device_os == 'android' ? 'appNameAndroid' : 'appNameIOS';
            $token = $this->device_os == 'android' ? $this->push_token : strtoupper($this->push_token);
            if ($this->device_os == 'android') {
                PushNotification::app($os)
                    ->to($token)
                    ->send($message);
            } else {
                $message = PushNotification::Message($message, array(
                    'badge' => $id,
                    'sound' => 'example.aiff',

                    'actionLocKey' => 'Action button title!',
                    'locKey' => 'localized key',
                    'locArgs' => array(
                        'localized args',
                        'localized args',
                    ),
                    'launchImage' => 'image.jpg',

                    'custom' => array('custom data' => array(
                        'id' => $id,
                    ))
                ));
                PushNotification::app($os)
                    ->to($token)
                    ->send($message);
            }
        }
    }
}
