<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Types extends Model
{
    protected $guarded = ['id'];

    protected $hidden = ['updated_at', 'created_at'];

}