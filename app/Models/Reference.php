<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Reference extends Model
{
    protected $guarded = ['id'];

    protected $hidden = ['updated_at', 'created_at'];

    public static function getCarTypes()
    {
       return self::returnSelf(3);
    }

    public static function getCarBrands()
    {
        return self::returnSelf(1);
    }

    public static function getCarModels($brand_id = false)
    {
        if ($brand_id)
        {
            return self::where('type_id', 2)->where('parent_id', $brand_id)->get(['id', self::getName('name')]);
        }
        return self::returnSelf(2);
    }

    public static function getName($field_name) {
        return $field_name . '_' . App::getLocale();
    }

    public static function returnSelf($id) {
        return self::where('type_id', $id)->get(['id', self::getName('name')]);
    }

    public function parent() {
        return $this->hasOne('App\Models\Reference', 'id', 'parent_id');
    }
}