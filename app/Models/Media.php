<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    public $guarded = ['id'];
    public $hidden = ['updated_at', 'created_at'];
}
