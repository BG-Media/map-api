<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{
    public $guarded = ['id'];
    public $table = 'ads';
    public $timestamps = false;
}
