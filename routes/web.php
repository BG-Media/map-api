<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Authorization, Content-Type');

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Dingo\Api\Routing\Helpers;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/_token', function () {
    return csrf_token();
});

Route::group(['prefix' => 'admin'], function () {

    Route::group(['prefix' => 'account'], function () {
        Route::post('auth', 'Admin\AccountController@auth');
    });

    Route::group(['middleware' => 'checkAdmin'], function () {
        Route::group(['prefix' => 'index'], function () {
            Route::get('digest', 'Admin\AccountController@digest')->name('adminIndex');
        });

        Route::resource('types', 'Admin\TypesController');

        Route::model('terms', App\Models\Reference::class);
        Route::resource('terms', 'Admin\TermsController',  [
            'as' => 'admin'
        ]);

        Route::resource('pages', 'Admin\PagesController',  [
            'as' => 'admin'
        ]);

        Route::resource('news', 'Admin\NewsController',  [
            'as' => 'admin'
        ]);

        Route::resource('ads', 'Admin\AdsController',  [
            'as' => 'admin'
        ]);

        Route::group(['prefix' => 'tariiff'], function () {
            Route::get('add', 'Admin\TariffController@add')->name('admin.tariff.add');
            Route::get('index', 'Admin\TariffController@index')->name('admin.tariff.index');
            Route::post('store', 'Admin\TariffController@store')->name('admin.tariff.store');
            Route::get('edit/{id}', 'Admin\TariffController@edit')->name('admin.tariff.edit');
            Route::put('update/{id}', 'Admin\TariffController@update')->name('admin.tariff.update');
            Route::get('show/{id}', 'Admin\TariffController@show')->name('admin.tariff.show');
            Route::delete('destroy/{id}', 'Admin\TariffController@destroy')->name('admin.tariff.destroy');
        });

        Route::group(['prefix' => 'currency'], function () {
            Route::get('add', 'Admin\TariffController@addMoney')->name('admin.currency.add');
            Route::post('store', 'Admin\TariffController@storeMoney')->name('admin.currency.store');
            Route::get('edit/{id}', 'Admin\TariffController@editMoney')->name('admin.currency.edit');
            Route::put('update/{id}', 'Admin\TariffController@updateMoney')->name('admin.currency.update');
            Route::get('show/{id}', 'Admin\TariffController@showMoney')->name('admin.currency.show');
            Route::delete('destroy/{id}', 'Admin\TariffController@destroyMoney')->name('admin.currency.destroy');
        });

        Route::model('customer','App\Models\Customer');
        Route::resource('customer', 'Admin\CustomerController',  [
            'as' => 'admin'
        ]);

        Route::group(['prefix' => 'transporter'], function () {
            Route::get('addBalance', 'Admin\TransporterController@formBalance')->name('admin.transporter.formBalance');
            Route::post('addBalance', 'Admin\TransporterController@updateBalance')->name('admin.transporter.updateBalance');

            Route::get('findLocation', 'Admin\TransporterController@findLocation')->name('admin.transporter.findLocation');
            Route::get('approveLocation/{order}', 'Admin\TransporterController@approveLocation')->name('admin.transporter.approveLocation');
            Route::get('deleteLocation/{order}', 'Admin\TransporterController@deleteLocation')->name('admin.transporter.deleteLocation');

            Route::get('deviceChange', 'Admin\TransporterController@deviceChange')->name('admin.transporter.deviceChange');
            Route::get('approveDevice/{token}', 'Admin\TransporterController@approveDevice')->name('admin.transporter.approveDevice');
            Route::get('deleteDevice/{token}', 'Admin\TransporterController@deleteDevice')->name('admin.transporter.deleteDevice');


            Route::get('block/{user}', 'Admin\TransporterController@block')->name('admin.transporter.block');
            Route::post('blockInit/{user}', 'Admin\TransporterController@blockInit')->name('admin.transporter.blockInit');
        });

        Route::model('transporter','App\Models\Transporter');
        Route::resource('transporter', 'Admin\TransporterController',  [
            'as' => 'admin'
        ]);

        Route::group(['prefix' => 'orders'], function () {
            Route::post('punishing/{id}', 'Admin\OrderController@punishing')->name('admin.orders.punishing');
            Route::get('punish/{id}', 'Admin\OrderController@punish')->name('admin.orders.punish');
            Route::get('moderate/{id}', 'Admin\OrderController@moderate')->name('admin.orders.moderate');
            Route::get('status/{id}', 'Admin\OrderController@byStatus')->name('admin.orders.byStatus');
            Route::get('customer/{id}', 'Admin\OrderController@byCustomer')->name('admin.orders.byCustomer');
            Route::get('transporter/{id}', 'Admin\OrderController@byTransporter')->name('admin.orders.byTransporter');
        });


        Route::resource('order', 'Admin\OrderController',  [
            'as' => 'admin'
        ]);

        Route::resource('location', 'Admin\LocationsController',  [
            'as' => 'admin'
        ]);
    });
});



$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['prefix' => 'v1'], function ($api) {

        $api->group(['prefix' => 'news'], function ($api) {
            $api->get('all', 'App\Http\Controllers\NewsController@all');
            $api->get('fetch/{news}', 'App\Http\Controllers\NewsController@fetch');
        });

        $api->group(['prefix' => 'pages'], function ($api) {
            $api->get('about', 'App\Http\Controllers\PagesController@about');
            $api->get('help', 'App\Http\Controllers\PagesController@help');
            $api->get('map', 'App\Http\Controllers\PagesController@map');
        });


        $api->group(['prefix' => 'customer'], function ($api) {
            $api->group(['prefix' => 'ads'], function ($api) {
                $api->post('list', 'App\Http\Controllers\Customer\AdsController@listing');
            });

            $api->group(['prefix' => 'account'], function ($api) {

                $api->post('signUp', 'App\Http\Controllers\Customer\AccountController@signUp');
                $api->post('signIn', 'App\Http\Controllers\Customer\AccountController@signIn');
                $api->post('resetPassword', 'App\Http\Controllers\Customer\AccountController@resetPassword');

                $api->group(['middleware' => 'checkWeb'], function ($api) {
                    $api->post('fetch', 'App\Http\Controllers\Customer\AccountController@fetch');
                    $api->post('update', 'App\Http\Controllers\Customer\AccountController@updateProfile');
                    $api->post('setPassword', 'App\Http\Controllers\Customer\AccountController@setPassword');
                });
            });

            $api->group(['prefix' => 'orders'], function ($api) {
                $api->get('references', 'App\Http\Controllers\Customer\OrdersController@references');
                $api->get('tariff', 'App\Http\Controllers\Customer\OrdersController@tariff');

                $api->group(['middleware' => 'checkWeb'], function ($api) {
                    $api->post('add', 'App\Http\Controllers\Customer\OrdersController@addOrder');
                    $api->post('my', 'App\Http\Controllers\Customer\OrdersController@myOrders');
                    $api->post('fetch/{id}', 'App\Http\Controllers\Customer\OrdersController@fetch');
                    $api->post('complete', 'App\Http\Controllers\Customer\OrdersController@complete');
                    $api->post('findLocation/{id}', 'App\Http\Controllers\Customer\OrdersController@findLocation');
                });
            });
        });


        $api->group(['prefix' => 'transporter'], function ($api) {
            $api->group(['prefix' => 'account'], function ($api) {
                $api->post('signUp', 'App\Http\Controllers\Transporter\AccountController@signUp');
                $api->post('signIn', 'App\Http\Controllers\Transporter\AccountController@signIn');
                $api->post('resetPassword', 'App\Http\Controllers\Transporter\AccountController@resetPassword');

                $api->group(['middleware' => 'checkDevice'], function ($api) {
                    $api->any('fetch', 'App\Http\Controllers\Transporter\AccountController@fetch');
                    $api->any('update', 'App\Http\Controllers\Transporter\AccountController@updateProfile');
                    $api->post('setPassword', 'App\Http\Controllers\Transporter\AccountController@setPassword');
                    $api->post('notifications', 'App\Http\Controllers\Transporter\AccountController@notifications');
                });
            });

            $api->group(['prefix' => 'cars', 'middleware' => 'checkDevice'], function ($api) {
                $api->post('myCars', 'App\Http\Controllers\Transporter\CarsController@myCars');
                $api->post('fetchCar', 'App\Http\Controllers\Transporter\CarsController@fetchCar');
                $api->post('addCar', 'App\Http\Controllers\Transporter\CarsController@addCar');
                $api->post('addImage', 'App\Http\Controllers\Transporter\CarsController@addImage');
                $api->any('listOptions', 'App\Http\Controllers\Transporter\CarsController@listOptions');
            });

            $api->group(['prefix' => 'filter', 'middleware' => 'checkDevice'], function ($api) {
                $api->post('preLoad', 'App\Http\Controllers\Transporter\OrdersController@preLoad');
                $api->post('getCount', 'App\Http\Controllers\Transporter\OrdersController@getCount');
                $api->post('getList', 'App\Http\Controllers\Transporter\OrdersController@getList');
            });

            $api->group(['prefix' => 'order', 'middleware' => 'checkDevice'], function ($api) {
                $api->post('takeOrder', 'App\Http\Controllers\Transporter\OrdersController@takeOrder');
                $api->post('delivered', 'App\Http\Controllers\Transporter\OrdersController@delivered');

                $api->post('myOrders', 'App\Http\Controllers\Transporter\OrdersController@myOrders');
                $api->post('latest', 'App\Http\Controllers\Transporter\OrdersController@latest');
                $api->post('find', 'App\Http\Controllers\Transporter\OrdersController@find');
            });

            $api->group(['prefix' => 'locate', 'middleware' => 'checkDevice'], function ($api) {
                $api->post('send', 'App\Http\Controllers\Transporter\OrdersController@sendLocation');
                $api->post('cancel', 'App\Http\Controllers\Transporter\OrdersController@cancelLocation');
            });
        });
    });

});