@extends('dashboard.layout')
@section('content')

    <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Home</li>
            <li class="breadcrumb-item"><a href="#">Admin</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
            <!-- Breadcrumb Menu-->
            <li class="breadcrumb-menu">
                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                    <a class="btn btn-secondary" href="#"><i class="icon-speech"></i></a>
                    <a class="btn btn-secondary" href="./"><i class="icon-graph"></i> &nbsp;Dashboard</a>
                    <a class="btn btn-secondary" href="#"><i class="icon-settings"></i> &nbsp;Баннеры</a>
                </div>
            </li>
        </ol>
        <div class="container-fluid">
            <div class="animated fadeIn">

                <!--/row-->

                <!--/row-->
                <div class="row">
                    <div class="col-lg-12">
                        <form method="post" action="{{route('admin.news.update', ['id' => $news->id])}}" class="card">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="card-header">
                                <i class="fa fa-align-justify"></i> Новость "{{ $news->title_ru }}"
                            </div>
                            <div class="card-block">
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="title_ru">Russian</label>
                                    <div class="col-md-9">
                                        <input type="text" id="title_en" class="form-control" name="title_ru" value="{{ $news->title_ru }}" placeholder="Название новости на русском" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="title_kk">Kazakh</label>
                                    <div class="col-md-9">
                                        <input type="text" id="title_kk" class="form-control" name="title_kk" value="{{ $news->title_kk }}" placeholder="Название новости на казахском" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="title_en">English</label>
                                    <div class="col-md-9">
                                        <input type="text" id="title_en" class="form-control" name="title_en" value="{{ $news->title_en }}" placeholder="Название новости на английском" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="content_ru">Содержание на русском</label>
                                    <div class="col-md-9">
                                        <textarea id="content_ru" name="content_ru" rows="9" class="form-control trumbowyg" placeholder="">{!! $news->content_ru  !!}</textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="content_kk">Содержание на казахском</label>
                                    <div class="col-md-9">
                                        <textarea id="content_kk" name="content_kk" rows="9" class="form-control trumbowyg" placeholder="">{!! $news->content_kk  !!}</textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="content_en">Содержание на английском</label>
                                    <div class="col-md-9">
                                        <textarea id="content_en" name="content_en" rows="9" class="form-control trumbowyg" placeholder="">{!! $news->content_en  !!}</textarea>
                                    </div>
                                </div>


                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i> Сохранить</button>
                                <a href="{{ route('admin.news.show', ['page' => $news->id]) }}" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i> Удалить</a>
                            </div>
                        </form>
                    </div>
                    <!--/col-->
                </div>
                <!--/row-->
            </div>
        </div>
        <!-- /.conainer-fluid -->
    </main>

@endsection