@extends('dashboard.layout')
@section('content')

    <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Home</li>
            <li class="breadcrumb-item"><a href="#">Admin</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
            <!-- Breadcrumb Menu-->
            <li class="breadcrumb-menu">
                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                    <a class="btn btn-secondary" href="#"><i class="icon-speech"></i></a>
                    <a class="btn btn-secondary" href="./"><i class="icon-graph"></i> &nbsp;Dashboard</a>
                    <a class="btn btn-secondary" href="#"><i class="icon-settings"></i> &nbsp;Баннеры</a>
                </div>
            </li>
        </ol>
        <div class="container-fluid">
            <div class="animated fadeIn">
                @for ($i = 1; $i < 5; $i++)
                    <div class="card">
                        <div class="card-header">№{{ $i }}</div>
                        <div class="card-block">
                            <div class="row">
                                <div class="col-md-4">
                                    <form method="post" action="{{ route('admin.ads.update', ['ads' => $i]) }}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        {{ method_field('PUT') }}
                                        <div class="form-group">
                                            URL: <input type="text" class="form-control" name="link" value="{{ $list[$i-1]->link }}" placeholder="Ссылка" required>
                                        </div>
                                        <div class="form-group">
                                            Image: <input type="file" class="form-control" name="image">
                                        </div>
                                        <div>
                                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i> Сохранить</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-md-8">
                                    <a href="{{ $list[$i-1]->link }}" target="_blank">
                                        <img src="{{ $list[$i-1]->image }}">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endfor


                <!--/row-->
            </div>
        </div>
        <!-- /.conainer-fluid -->
    </main>

@endsection