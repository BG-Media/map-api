@extends('dashboard.layout')
@section('content')

    <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Home</li>
            <li class="breadcrumb-item"><a href="#">Admin</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
            <!-- Breadcrumb Menu-->
            <li class="breadcrumb-menu">
                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                    <a class="btn btn-secondary" href="#"><i class="icon-speech"></i></a>
                    <a class="btn btn-secondary" href="./"><i class="icon-graph"></i> &nbsp;Dashboard</a>
                    <a class="btn btn-secondary" href="#"><i class="icon-settings"></i> &nbsp;Грузы</a>
                </div>
            </li>
        </ol>
        <div class="container-fluid">
            <div class="animated fadeIn">

                <!--/row-->

                <!--/row-->
                <div class="row">

                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-align-justify"></i> Грузы заказчика {{ $customer->name }}
                            </div>
                            <div class="card-block">
                                <table class="table table-bordered table-striped table-condensed">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Пункт отправления</th>
                                        <th>Пункт назначения</th>
                                        <th>Статус</th>
                                        <th>Перевозчик</th>
                                        <th>Действия</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($orders as $order)
                                        <tr>
                                            <td>{{ $order->id }}</td>
                                            <td>{{ $order->locationA->address_ru }}</td>
                                            <td>{{ $order->locationB->address_ru }}</td>
                                            <td>{{ $statuses[$order->status] }}</td>
                                            <td>
                                                @if ($order->transporter_id > 0)
                                                    <a href="{{ route('admin.transporter.edit', ['transporter' => $order->transporter->id])}}">
                                                        {{ $order->transporter->name }}
                                                    </a>
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('admin.order.edit', ['order' => $order->id]) }}"
                                                   class="btn btn-warning"><i class="icon-pencil"></i> Изменить</a>
                                                <a href="{{ route('admin.order.show', ['order' => $order->id]) }}"
                                                   class="btn btn-danger"><i class="icon-trash"></i> Удалить</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--/col-->
                </div>
                <!--/row-->
            </div>
        </div>
        <!-- /.conainer-fluid -->
    </main>

@endsection