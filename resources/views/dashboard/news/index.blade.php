@extends('dashboard.layout')
@section('content')

    <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Home</li>
            <li class="breadcrumb-item"><a href="#">Admin</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
            <!-- Breadcrumb Menu-->
            <li class="breadcrumb-menu">
                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                    <a class="btn btn-secondary" href="#"><i class="icon-speech"></i></a>
                    <a class="btn btn-secondary" href="./"><i class="icon-graph"></i> &nbsp;Dashboard</a>
                    <a class="btn btn-secondary" href="#"><i class="icon-settings"></i> &nbsp;Новости</a>
                </div>
            </li>
        </ol>
        <div class="container-fluid">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-xs-12 col-md-12"><a href="{{ route('admin.news.create') }}" class="btn btn-success btn-add"><i class="fa fa-plus"></i>&nbsp; Добавить новую</a></div>

                    @foreach($news as $item)
                        <div class="col-sm-6 col-md-4">
                            <div class="card card-default">
                                <div class="card-header">
                                    {{ mb_substr($item->title_ru, 0, 50) }}
                                    <div class="card-actions">
                                        <a href="{{ route('admin.news.edit', ['news' => $item->id]) }}"><i class="icon-pencil"></i></a>
                                    </div>
                                </div>
                                <div class="card-block">
                                    {{ mb_substr($item->content_ru, 0, 200) }}...
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <!--/row-->
            </div>
        </div>
        <!-- /.conainer-fluid -->
    </main>

@endsection