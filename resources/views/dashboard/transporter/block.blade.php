@extends('dashboard.layout')
@section('content')

    <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Home</li>
            <li class="breadcrumb-item"><a href="#">Admin</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
            <!-- Breadcrumb Menu-->
            <li class="breadcrumb-menu">
                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                    <a class="btn btn-secondary" href="#"><i class="icon-speech"></i></a>
                    <a class="btn btn-secondary" href="./"><i class="icon-graph"></i> &nbsp;Dashboard</a>
                    <a class="btn btn-secondary" href="#"><i class="icon-settings"></i> &nbsp;Перевозчик</a>
                </div>
            </li>
        </ol>
        <div class="container-fluid">
            <div class="animated fadeIn">

                <!--/row-->

                <!--/row-->
                <div class="row">
                    <div class="col-lg-12">
                        <form method="post"
                              action="{{route('admin.transporter.blockInit', ['transporter' => $user->id])}}"
                              class="card">
                            {{ csrf_field() }}
                            <div class="card-header">
                                <i class="fa fa-align-justify"></i> Перевозчик "{{ $user->name }}"
                            </div>
                            <div class="card-block">
                                <h5>Заблокировать пользователя {{ $user->name }} на</h5>

                                @if ($user->blockTime)
                                    <p>
                                        @if ($user->blockTime < time())
                                            Блок закончился в
                                        @else
                                            Блок закончится
                                        @endif

                                        {{ date('d.m.Y, H:i', $user->blockTime) }}
                                    </p>
                                @endif

                                <div class="row">
                                    <div class="form-group col-md-2">
                                        <div class="">
                                            <input type="number" class="form-control" name="number" min="1"
                                                   placeholder="Введите цифру">
                                        </div>
                                    </div>
                                    <div class="form-group  col-md-1">
                                        <div class="">
                                            <select name="unit" class="form-control">
                                                <option value="hour">час.</option>
                                                <option value="day">день</option>
                                                <option value="week">неделя</option>
                                                <option value="month">мес.</option>
                                                <option value="year">год.</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card-footer">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i>
                                    Блокировка
                                </button>
                            </div>
                        </form>
                    </div>
                    <!--/col-->
                </div>
                <!--/row-->
            </div>
        </div>
        <!-- /.conainer-fluid -->
    </main>

@endsection