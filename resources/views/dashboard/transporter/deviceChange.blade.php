@extends('dashboard.layout')
@section('content')

    <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('adminIndex') }}">Панель управления</a></li>
            <li class="breadcrumb-item active">Смена устройства</li>
        </ol>
        <div class="container-fluid">
            <div class="animated fadeIn">
                <div class="row">
                    @foreach($list as $item)
                        <div class="col-sm-6 col-md-4">
                            <div class="card card-default">
                                <div class="card-block">

                                    <h3 class="card-title">Запрос на смену устройства | №{{$item->id}}</h3>
                                    @if (empty($item->user))
                                        <p class="card-text">Ошибка в запросе. Возможно нет пользователя
                                            ID {{ $item->user_id }}</p>
                                    @else
                                        <p class="card-text"><b>Пользователь:</b> <a
                                                    href="{{ route('admin.transporter.edit', ['user' => $item->user->id]) }}">{{ $item->user->name }}</a>
                                        </p>
                                        <p class="card-text"><b>Кодовое слово:</b> {{ $item->user->security_code }}</p>
                                        <p class="card-text"><b>Старое
                                                устройство:</b> {{ \App\Models\Token::where('token', $item->user->device_token)->first()->device }}
                                        </p>
                                        <p class="card-text"><b>Новое устройство:</b> {{ $item->device }}</p>
                                        <div>
                                            <a href="{{ route('admin.transporter.approveDevice', ['token' => $item->id]) }}"
                                               class="btn btn-primary">Сменить устройство</a>
                                            <a href="{{ route('admin.transporter.deleteDevice', ['token' => $item->id]) }}"
                                               class="btn btn-primary">Отклонить запрос</a>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                @endforeach
                <!--/row-->
                </div>
            </div>
        </div>
        <!-- /.conainer-fluid -->
    </main>

@endsection