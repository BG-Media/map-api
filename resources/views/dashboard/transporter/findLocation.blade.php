@extends('dashboard.layout')
@section('content')

    <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('adminIndex') }}">Панель управления</a></li>
            <li class="breadcrumb-item active">Запрос местоположения</li>
        </ol>
        <div class="container-fluid">
            <div class="animated fadeIn">
                <div class="row">
                    @foreach($list as $item)
                        <div class="col-sm-6 col-md-4">
                            <div class="card card-default">
                                <div class="card-block">
                                    <h3 class="card-title">Запрос местоположения груза №{{$item->order_id}}</h3>
                                    <p class="card-text">Дата запроса: {{ date('d.m.Y', $item->time) }}</p>
                                    <p class="card-text">
                                        Заказчик:
                                        <a href="{{ route('admin.customer.edit', ['customer' => $item->order->customer]) }}">
                                            ID  {{ $item->order->customer->id }}
                                            {{ $item->order->customer->name }}
                                        </a>
                                    </p>
                                    <p class="card-text">
                                        Перевозчик:
                                        <a href="{{ route('admin.transporter.edit', ['transporter' => $item->order->transporter]) }}">
                                            ID  {{ $item->order->transporter->id }}
                                            {{ $item->order->transporter->name }}
                                        </a>
                                    </p>
                                    <div>

                                        <a href="{{ route('admin.transporter.approveLocation', ['order' => $item->id]) }}" class="btn btn-primary">Подтвердить запрос</a>
                                        <a href="{{ route('admin.transporter.deleteLocation', ['order' => $item->id]) }}" class="btn btn-primary">Удалить запрос</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                @endforeach
                <!--/row-->
                </div>
            </div>
        </div>
        <!-- /.conainer-fluid -->
    </main>

@endsection