@extends('dashboard.layout')
@section('content')

    <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('adminIndex') }}">Панель управления</a></li>
            <li class="breadcrumb-item active">Перевозчики</li>
        </ol>
        <div class="container-fluid">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                Перевозчики
                            </div>
                            <div class="card-block">

                                <!--/.row-->
                                <br>
                                <form method="post" class="form-group row" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <label class="col-md-2 form-control-label" for="file-input">Выберите excel файл (xls, xlsx)</label>
                                    <div class="col-md-4">
                                        <input type="file" id="file-input" name="dump" accept="text/csv, .csv, .xls, .xlsx">
                                    </div>
                                    <div class="col-md-12">
                                        <button class="btn btn-primary">Загрузить</button>
                                    </div>
                                </form>
                                <a class="btn btn-link" href="/sample.xlsx"><i class="fa fa-link"></i>&nbsp; Скачать пример файла</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/row-->
            </div>
        </div>
        <!-- /.conainer-fluid -->
    </main>

@endsection