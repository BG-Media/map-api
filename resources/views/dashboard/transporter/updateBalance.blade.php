@extends('dashboard.layout')
@section('content')

    <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('adminIndex') }}">Панель управления</a></li>
            <li class="breadcrumb-item active">Перевозчики</li>
        </ol>
        <div class="container-fluid">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                Перевозчики
                            </div>
                            <div class="card-block">

                                <!--/.row-->

                                @foreach($errors as $error)
                                    <button type="button" class="btn btn-warning btn-lg btn-block">{{ $error }}</button>
                                @endforeach
                                <br>

                                <table class="table table-hover table-outline mb-0 hidden-sm-down">
                                    <thead class="thead-default">
                                    <tr>
                                        <th>ID</th>
                                        <th>НАЗВАНИЕ/ИМЯ</th>
                                        <th class="text-center">БАЛАНС</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $user)
                                        <tr>
                                            <td>{{ $user->id }}</td>
                                            <td>{{ $user->name }}</td>
                                            <td class="text-center">{{ $user->balance }} <span class="tag tag-success">+{{ $user->newBalance }}</span></td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/row-->
            </div>
        </div>
        <!-- /.conainer-fluid -->
    </main>

@endsection