@extends('dashboard.layout')
@section('content')

    <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Home</li>
            <li class="breadcrumb-item"><a href="#">Admin</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
            <!-- Breadcrumb Menu-->
            <li class="breadcrumb-menu">
                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                    <a class="btn btn-secondary" href="#"><i class="icon-speech"></i></a>
                    <a class="btn btn-secondary" href="./"><i class="icon-graph"></i> &nbsp;Dashboard</a>
                    <a class="btn btn-secondary" href="#"><i class="icon-settings"></i> &nbsp;Изменить запись</a>
                </div>
            </li>
        </ol>
        <div class="container-fluid">
            <div class="animated fadeIn">

                <!--/row-->

                <!--/row-->
                <div class="row">
                    <div class="col-lg-5">
                        <form method="post" action="{{route('admin.tariff.update', ['tariff' => $tariff->id])}}"
                              class="card">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="card-header">
                                <i class="fa fa-align-justify"></i> Изменить запись {{ $tariff->name }}
                            </div>
                            <div class="card-block">
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <h5 class="h5">Дата действия с</h5>
                                    </div>
                                    <div class="form-group col-sm-2">
                                        <label for="city">Число</label>
                                        <input type="number" id="label" class="form-control" name="start_day"
                                               value="{{ $tariff->start_day }}" placeholder="Число" min="1" max="31" required>
                                    </div>
                                    <div class="form-group col-sm-2">
                                        <label for="postal-code">Месяц</label>
                                        <input type="number" id="label" class="form-control" name="start_month"
                                               value="{{ $tariff->start_month }}" placeholder="Месяц" min="1" max="12" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-6">
                                        <h5 class="h5">Дата действия по</h5>
                                    </div>
                                    <div class="form-group col-sm-2">
                                        <label for="city">Число</label>
                                        <input type="number" id="label" class="form-control" name="end_day"
                                               value="{{ $tariff->end_day }}" placeholder="Число" min="1" max="31" required>
                                    </div>
                                    <div class="form-group col-sm-2">
                                        <label for="postal-code">Месяц</label>
                                        <input type="number" id="label" class="form-control" name="end_month"
                                               value="{{ $tariff->end_month }}" placeholder="Месяц" min="1" max="12" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="tenge">Наименование</label>
                                    <div class="col-md-9">
                                        <input type="text" id="tenge" class="form-control" name="name"
                                               value="{{ $tariff->name }}" placeholder="Перевозка" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="">Средний погр. вес, тонн</label>
                                    <div class="col-md-9">
                                        <input type="number" id="" class="form-control" name="avg_weight"
                                               value="{{ $tariff->avg_weight }}" placeholder="" min="1" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="">Средний погр. объем, м3</label>
                                    <div class="col-md-9">
                                        <input type="number" id="" class="form-control" name="avg_capacity"
                                               value="{{ $tariff->avg_capacity }}" placeholder="" min="1" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="">Стоимость км перевозки</label>
                                    <div class="col-md-9">
                                        <input type="number" id="" class="form-control" name="price"
                                               value="{{ $tariff->price }}" placeholder="" min="1" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="">Отклн. от рек. стоимости (%)</label>
                                    <div class="col-md-9">
                                        <input type="number" id="" class="form-control" name="dumping"
                                               value="{{ $tariff->dumping }}" placeholder="" step="0.01" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="">Валюта</label>
                                    <div class="col-md-9">
                                        <select name="money" class="form-control" required>
                                            @foreach($currencyList as $money)
                                                <option value="{{ $money->label }}"
                                                        @if($money->label == $tariff->money ) selected @endif>{{ $money->label }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="">Тип транспорта</label>
                                    <div class="col-md-9">
                                        <select name="car_types" class="form-control" required>
                                            @foreach($carTypes as $item)
                                                <option value="{{ $item->id }}"
                                                    @if ($item->id == $tariff->car_types) selected @endif>{{ $item->name_ru }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="">k1</label>
                                    <div class="col-md-9">
                                        <input type="number" step="0.1" id="" class="form-control" name="k1"
                                               value="{{ $tariff->k1 }}" placeholder="" min="1" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="">k2</label>
                                    <div class="col-md-9">
                                        <input type="number" step="0.1" id="" class="form-control" name="k2"
                                               value="{{ $tariff->k2 }}" placeholder="" min="1" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="">k3</label>
                                    <div class="col-md-9">
                                        <input type="number" step="0.1" id="" class="form-control" name="k3"
                                               value="{{ $tariff->k3 }}" placeholder="" min="1" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="">k4</label>
                                    <div class="col-md-9">
                                        <input type="number" step="0.1" id="" class="form-control" name="k4"
                                               value="{{ $tariff->k4 }}" placeholder="" min="1" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="">k5</label>
                                    <div class="col-md-9">
                                        <input type="number" step="0.1" id="" class="form-control" name="k5"
                                               value="{{ $tariff->k5 }}" placeholder="" min="1" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="">k6</label>
                                    <div class="col-md-9">
                                        <input type="number" step="0.1" id="" class="form-control" name="k6"
                                               value="{{ $tariff->k6 }}" placeholder="" min="1" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="">k7</label>
                                    <div class="col-md-9">
                                        <input type="number" step="0.1" id="" class="form-control" name="k7"
                                               value="{{ $tariff->k7 }}" placeholder="" min="1" required>
                                    </div>
                                </div>

                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i>
                                    Изменить
                                </button>
                            </div>
                        </form>
                    </div>
                    <!--/col-->
                </div>
                <!--/row-->
            </div>
        </div>
        <!-- /.conainer-fluid -->
    </main>

@endsection