@extends('dashboard.layout')
@section('content')

    <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Home</li>
            <li class="breadcrumb-item"><a href="#">Admin</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
            <!-- Breadcrumb Menu-->
            <li class="breadcrumb-menu">
                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                    <a class="btn btn-secondary" href="#"><i class="icon-speech"></i></a>
                    <a class="btn btn-secondary" href="./"><i class="icon-graph"></i> &nbsp;Dashboard</a>
                    <a class="btn btn-secondary" href="#"><i class="icon-settings"></i> &nbsp;Тарифная ставка</a>
                </div>
            </li>
        </ol>
        <div class="container-fluid">
            <div class="animated fadeIn">

                <!--/row-->

                <!--/row-->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-align-justify"></i> Тарифная ставка. <a href="{{ route('admin.tariff.add') }}" class="">Добавить новую запись</a>
                            </div>
                            <div class="card-block">
                                <table class="table table-bordered table-striped table-condensed">
                                    <thead>
                                    <tr>
                                        <th>нпп</th>
                                        <th>Дата дейс-я с</th>
                                        <th>Дата дейс-я по</th>
                                        <th>Наименование</th>
                                        <th>Ср. погр. вес, тонн</th>
                                        <th>Ср. погр. объем, м3</th>
                                        <th>Ст. км перевозки</th>
                                        <th>Валюта</th>
                                        <th>% откл.</th>
                                        <th>Тип авто</th>
                                        <th>к1</th>
                                        <th>к2</th>
                                        <th>к3</th>
                                        <th>к4</th>
                                        <th>к5</th>
                                        <th>к6</th>
                                        <th>Действия</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($tariffs as $tariff)
                                        <tr>
                                            <td>{{ $tariff->id }}</td>
                                            <td>{{ $tariff->start_date }}</td>
                                            <td>{{ $tariff->end_date }}</td>
                                            <td>{{ $tariff->name }}</td>
                                            <td>{{ $tariff->avg_weight }}</td>
                                            <td>{{ $tariff->avg_capacity }}</td>
                                            <td>{{ $tariff->price }}</td>
                                            <td>{{ $tariff->money }}</td>
                                            <td>{{ $tariff->dumping }}</td>
                                            <td>{{ \App\Models\Reference::find($tariff->car_types)->name_ru }}</td>
                                            <td>{{ $tariff->k1 }}</td>
                                            <td>{{ $tariff->k2 }}</td>
                                            <td>{{ $tariff->k3 }}</td>
                                            <td>{{ $tariff->k4 }}</td>
                                            <td>{{ $tariff->k5 }}</td>
                                            <td>{{ $tariff->k6 }}</td>
                                            <td>
                                                <a href="{{ route('admin.tariff.edit', ['tariff' => $tariff->id]) }}" class=""><i class="icon-pencil"></i> Изм.</a>
                                                <a href="{{ route('admin.tariff.show', ['tariff' => $tariff->id]) }}" class=""><i class="icon-trash"></i> Удал.</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--/col-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-align-justify"></i> Валюта
                            </div>
                            <div class="card-block">
                                <table class="table table-bordered table-striped table-condensed">
                                    <thead>
                                    <tr>
                                        <th>№</th>
                                        <th>Дата</th>
                                        <th>Валюта</th>
                                        <th>В тенге</th>
                                        <th class="text-center"><a href="{{ route('admin.currency.add') }}" class="btn btn-success btn-link"><i class="fa fa-plus"></i> Добавить</a></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($currencyList as $money)
                                        <tr>
                                            <td>{{ $money->id }}</td>
                                            <td>{{ date('d.m', $money->date) }}</td>
                                            <td>{{ $money->label }}</td>
                                            <td>{{ $money->tenge }}</td>
                                            <td class="text-center">
                                                <a href="{{ route('admin.currency.edit', ['currency' => $money->id]) }}" class="btn btn-warning btn-link"><i class="icon-pencil"></i> Изменить</a>
                                                <a href="{{ route('admin.currency.show', ['currency' => $money->id]) }}" class="btn btn-danger btn-link"><i class="icon-trash"></i> Удалить</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--/col-->
                </div>
            </div>
        </div>
        <!-- /.conainer-fluid -->
    </main>

@endsection