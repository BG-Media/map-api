@extends('dashboard.layout')
@section('content')

    <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Home</li>
            <li class="breadcrumb-item"><a href="#">Admin</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
            <!-- Breadcrumb Menu-->
            <li class="breadcrumb-menu">
                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                    <a class="btn btn-secondary" href="#"><i class="icon-speech"></i></a>
                    <a class="btn btn-secondary" href="./"><i class="icon-graph"></i> &nbsp;Dashboard</a>
                    <a class="btn btn-secondary" href="#"><i class="icon-settings"></i> &nbsp;Изменить валюту</a>
                </div>
            </li>
        </ol>
        <div class="container-fluid">
            <div class="animated fadeIn">

                <!--/row-->

                <!--/row-->
                <div class="row">
                    <div class="col-lg-4">
                        <form method="post" action="{{route('admin.currency.update', ['money' => $money->id])}}"
                              class="card">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="card-header">
                                <i class="fa fa-align-justify"></i> Изменить валюту "{{ $money->label }}"
                            </div>
                            <div class="card-block">
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="label">Валюта</label>
                                    <div class="col-md-9">
                                        <input type="text" id="label" class="form-control" name="label"
                                               value="{{ $money->label }}" placeholder="Например, KZT">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="tenge">В тенге</label>
                                    <div class="col-md-9">
                                        <input type="text" id="tenge" class="form-control" name="tenge"
                                               value="{{ $money->tenge }}" placeholder="Через точку, например, 5.5">
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i>
                                    Изменить
                                </button>
                            </div>
                        </form>
                    </div>
                    <!--/col-->
                </div>
                <!--/row-->
            </div>
        </div>
        <!-- /.conainer-fluid -->
    </main>

@endsection