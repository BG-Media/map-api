@extends('dashboard.layout')
@section('content')

    <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Home</li>
            <li class="breadcrumb-item"><a href="#">Admin</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
            <!-- Breadcrumb Menu-->
            <li class="breadcrumb-menu">
                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                    <a class="btn btn-secondary" href="#"><i class="icon-speech"></i></a>
                    <a class="btn btn-secondary" href="./"><i class="icon-graph"></i> &nbsp;Dashboard</a>
                    <a class="btn btn-secondary" href="#"><i class="icon-settings"></i> &nbsp;Термины</a>
                </div>
            </li>
        </ol>
        <div class="container-fluid">
            <div class="animated fadeIn">

                <!--/row-->

                <!--/row-->
                <div class="row">
                    <div class="col-lg-12">
                        <form method="post" action="{{route('admin.terms.update', ['id' => $ref->id])}}"
                              class="card">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="card-header">
                                <i class="fa fa-align-justify"></i> Термин {{$ref->name_ru }}
                            </div>
                            <div class="card-block">
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="name_ru">Russian</label>
                                    <div class="col-md-9">
                                        <input type="text" id="name_ru" class="form-control" name="name_ru"
                                               value="{{$ref->name_ru }}" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="name_kk">Kazakh</label>
                                    <div class="col-md-9">
                                        <input type="text" id="name_kk" class="form-control" name="name_kk"
                                               value="{{$ref->name_kk }}" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="name_en">English</label>
                                    <div class="col-md-9">
                                        <input type="text" id="name_en" class="form-control" name="name_en"
                                               value="{{$ref->name_en }}" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="type_id">Тип данных</label>
                                    <div class="col-md-9">
                                        <select name="type_id" class="form-control" id="type_id">
                                            @foreach($types as $type)
                                                <option value="{{ $type->id }}" @if ($type->id == $ref->type_id) selected @endif>{{ $type->name_ru }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @if ($ref->type_id == 2)
                                    <div class="form-group row">
                                        <label class="col-md-3 form-control-label" for="parent_id">Модель</label>
                                        <div class="col-md-9">
                                            <select name="parent_id" class="form-control" id="parent_id">
                                                @foreach($models as $model)
                                                    <option value="{{ $model->id }}" @if ($model->id == $ref->parent_id) selected @endif>{{ $model->name_ru }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                @endif

                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i>
                                    Сохранить
                                </button>
                                <a href="{{ route('admin.terms.show', ['term' => $ref->id]) }}"
                                   class="btn btn-sm btn-danger"><i class="fa fa-ban"></i> Удалить</a>
                            </div>
                        </form>
                    </div>
                    <!--/col-->
                </div>
                <!--/row-->
            </div>
        </div>
        <!-- /.conainer-fluid -->
    </main>

@endsection