@extends('dashboard.layout')
@section('content')

    <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Home</li>
            <li class="breadcrumb-item"><a href="#">Admin</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
            <!-- Breadcrumb Menu-->
            <li class="breadcrumb-menu">
                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                    <a class="btn btn-secondary" href="#"><i class="icon-speech"></i></a>
                    <a class="btn btn-secondary" href="./"><i class="icon-graph"></i> &nbsp;Dashboard</a>
                    <a class="btn btn-secondary" href="#"><i class="icon-settings"></i> &nbsp;Термины</a>
                </div>
            </li>
        </ol>
        <div class="container-fluid">
            <div class="animated fadeIn">

                <!--/row-->

                <!--/row-->
                <div class="row">
                    <div class="col-xs-12 col-md-12"><a href="{{ route('admin.terms.create', ['type' => $type->id]) }}" class="btn btn-success btn-add"><i class="fa fa-plus"></i>&nbsp; Добавить термин</a></div>

                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-align-justify"></i> Термины типа данных "{{ $type->name_ru }}"
                            </div>
                            <div class="card-block">
                                <table class="table table-bordered table-striped table-condensed">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        @if ($type->id ==2) <th>Модель</th> @endif
                                        <th>Russian</th>
                                        <th>Kazakh</th>
                                        <th>English</th>
                                        <th>Действия</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($refs as $ref)
                                        <tr>
                                            <td>{{ $ref->id }}</td>
                                            @if ($type->id ==2) <td>{{ $ref->parent->name_ru }}</td> @endif
                                            <td>{{ $ref->name_ru }}</td>
                                            <td>{{ $ref->name_kk }}</td>
                                            <td>{{ $ref->name_en }}</td>
                                            <td>
                                                <a href="{{ route('admin.terms.edit', ['term' => $ref->id]) }}" class="btn btn-warning"><i class="icon-pencil"></i> Изменить</a>
                                                <a href="{{ route('admin.terms.show', ['term' => $ref->id]) }}" class="btn btn-danger"><i class="icon-trash"></i> Удалить</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--/col-->
                </div>
                <!--/row-->
            </div>
        </div>
        <!-- /.conainer-fluid -->
    </main>

@endsection