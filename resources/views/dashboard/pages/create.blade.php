@extends('dashboard.layout')
@section('content')

    <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Home</li>
            <li class="breadcrumb-item"><a href="#">Admin</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
            <!-- Breadcrumb Menu-->
            <li class="breadcrumb-menu">
                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                    <a class="btn btn-secondary" href="#"><i class="icon-speech"></i></a>
                    <a class="btn btn-secondary" href="./"><i class="icon-graph"></i> &nbsp;Dashboard</a>
                    <a class="btn btn-secondary" href="#"><i class="icon-settings"></i> &nbsp;Страницы</a>
                </div>
            </li>
        </ol>
        <div class="container-fluid">
            <div class="animated fadeIn">

                <!--/row-->

                <!--/row-->
                <div class="row">
                    <div class="col-lg-12">
                        <form method="post" action="{{route('admin.pages.store')}}" class="card">
                            {{ csrf_field() }}
                            <div class="card-header">
                                <i class="fa fa-align-justify"></i>Новая страница
                            </div>
                            <div class="card-block">
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="name_ru">Russian</label>
                                    <div class="col-md-9">
                                        <input type="text" id="name_ru" class="form-control" name="name_ru" placeholder="Название страницы на русском" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="name_kk">Kazakh</label>
                                    <div class="col-md-9">
                                        <input type="text" id="name_kk" class="form-control" name="name_kk" placeholder="Название страницы на казахском" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="name_en">English</label>
                                    <div class="col-md-9">
                                        <input type="text" id="name_en" class="form-control" name="name_en" placeholder="Название страницы на английском" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="content_ru">Содержание на русском</label>
                                    <div class="col-md-9">
                                        <textarea id="content_ru" name="content_ru" rows="9" class="form-control trumbowyg" placeholder=""></textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="content_kk">Содержание на казахском</label>
                                    <div class="col-md-9">
                                        <textarea id="content_kk" name="content_kk" rows="9" class="form-control trumbowyg" placeholder=""></textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="content_en">Содержание на английском</label>
                                    <div class="col-md-9">
                                        <textarea id="content_en" name="content_en" rows="9" class="form-control trumbowyg" placeholder=""></textarea>
                                    </div>
                                </div>


                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i> Добавить</button>
                            </div>
                        </form>
                    </div>
                    <!--/col-->
                </div>
                <!--/row-->
            </div>
        </div>
        <!-- /.conainer-fluid -->
    </main>

@endsection