@extends('dashboard.layout')
@section('content')

    <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Home</li>
            <li class="breadcrumb-item"><a href="#">Admin</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
            <!-- Breadcrumb Menu-->
            <li class="breadcrumb-menu">
                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                    <a class="btn btn-secondary" href="#"><i class="icon-speech"></i></a>
                    <a class="btn btn-secondary" href="./"><i class="icon-graph"></i> &nbsp;Dashboard</a>
                    <a class="btn btn-secondary" href="#"><i class="icon-settings"></i> &nbsp;Локации</a>
                </div>
            </li>
        </ol>
        <div class="container-fluid">
            <div class="animated fadeIn">

                <!--/row-->

                <!--/row-->
                <div class="row">

                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <i class="fa fa-align-justify"></i> Локации
                            </div>
                            <div class="card-block">
                                <table class="table table-bordered table-hover table-condensed">
                                    <thead class="thead-default">
                                    <tr>
                                        <th>ID</th>
                                        <th>НАЗВАНИЕ</th>
                                        <th>ПОЛНОЕ НАЗВАНИЕ</th>
                                        <th>MAP ID</th>
                                        <th>КООРДИНАТЫ</th>
                                        <th>ДЕЙСТВИЯ</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($locations as $item)
                                        <tr>
                                            <td>{{ $item->id }}</td>
                                            <td>{{ $item->label_ru }}</td>
                                            <td>{{ $item->address_ru }}</td>
                                            <td>{{ $item->map_id }}</td>
                                            <td>{{ $item->coords }}</td>
                                            <td>
                                                <a href="{{ route('admin.location.edit', ['term' => $item->id]) }}"
                                                   class="btn btn-warning"><i class="icon-pencil"></i> Изменить</a>
                                                <a href="{{ route('admin.location.show', ['term' => $item->id]) }}"
                                                   class="btn btn-danger"><i class="icon-trash"></i> Удалить</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--/col-->
                </div>
                <!--/row-->
            </div>
        </div>
        <!-- /.conainer-fluid -->
    </main>

@endsection