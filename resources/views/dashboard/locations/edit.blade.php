@extends('dashboard.layout')
@section('content')

    <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Home</li>
            <li class="breadcrumb-item"><a href="#">Admin</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
            <!-- Breadcrumb Menu-->
            <li class="breadcrumb-menu">
                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                    <a class="btn btn-secondary" href="#"><i class="icon-speech"></i></a>
                    <a class="btn btn-secondary" href="./"><i class="icon-graph"></i> &nbsp;Dashboard</a>
                    <a class="btn btn-secondary" href="#"><i class="icon-settings"></i> &nbsp;Локации</a>
                </div>
            </li>
        </ol>
        <div class="container-fluid">
            <div class="animated fadeIn">

                <!--/row-->

                <!--/row-->
                <div class="row">
                    <div class="col-lg-12">
                        <form method="post" action="{{route('admin.location.update', ['id' => $location->id])}}"
                              class="card">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="card-header">
                                <i class="fa fa-align-justify"></i> Локация {{$location->label_ru }}
                            </div>
                            <div class="card-block">
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <div id="map" style="width: 100%; height: 300px;"></div>

                                        <script type="text/javascript">
                                            ymaps.ready(function(){
                                                var map = new ymaps.Map("map", {
                                                    center: [{{ $location->coords }}],
                                                    zoom: 12
                                                });
                                            });

                                        </script>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="name_ru">Название (RU)</label>
                                    <div class="col-md-9">
                                        <input type="text" id="name_ru" class="form-control" name="label_ru"
                                               value="{{$location->label_ru }}" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="label_kz">Название (KZ)</label>
                                    <div class="col-md-9">
                                        <input type="text" id="name_kk" class="form-control" name="label_kz"
                                               value="{{$location->label_kz }}" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="label_en">Название (EN)</label>
                                    <div class="col-md-9">
                                        <input type="text" id="name_en" class="form-control" name="label_en"
                                               value="{{$location->label_en }}" placeholder="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="address_ru">Полное название (RU)</label>
                                    <div class="col-md-9">
                                        <input type="text" id="address_ru" class="form-control" name="address_ru"
                                               value="{{$location->address_ru }}" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="address_kz">Полное название (KZ)</label>
                                    <div class="col-md-9">
                                        <input type="text" id="address_kz" class="form-control" name="address_kz"
                                               value="{{$location->address_kz }}" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="address_en">Полное название (EN)</label>
                                    <div class="col-md-9">
                                        <input type="text" id="address_en" class="form-control" name="address_en"
                                               value="{{$location->address_en }}" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="map_id">MAP ID (системное)</label>
                                    <div class="col-md-9">
                                        <input type="text" id="map_id" class="form-control" name=""
                                               value="{{$location->map_id }}" placeholder="" disabled>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="coords">Координаты (системное)</label>
                                    <div class="col-md-9">
                                        <input type="text" id="coords" class="form-control" name=""
                                               value="{{$location->coords }}" placeholder="" disabled>

                                    </div>
                                </div>


                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i>
                                    Сохранить
                                </button>
                                <a href="{{ route('admin.terms.show', ['term' => $location->id]) }}"
                                   class="btn btn-sm btn-danger"><i class="fa fa-ban"></i> Удалить</a>
                            </div>
                        </form>
                    </div>
                    <!--/col-->
                </div>
                <!--/row-->
            </div>
        </div>
        <!-- /.conainer-fluid -->
    </main>

@endsection