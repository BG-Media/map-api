@extends('dashboard.layout')
@section('content')

    <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Home</li>
            <li class="breadcrumb-item"><a href="#">Admin</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
            <!-- Breadcrumb Menu-->
            <li class="breadcrumb-menu">
                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                    <a class="btn btn-secondary" href="#"><i class="icon-speech"></i></a>
                    <a class="btn btn-secondary" href="./"><i class="icon-graph"></i> &nbsp;Dashboard</a>
                    <a class="btn btn-secondary" href="#"><i class="icon-settings"></i> &nbsp;Заказчики</a>
                </div>
            </li>
        </ol>
        <div class="container-fluid">
            <div class="animated fadeIn">

                <!--/row-->

                <!--/row-->
                <div class="row">
                    <div class="col-lg-12">
                        <form method="post" action="{{route('admin.customer.update', ['id' => $user->id])}}"
                              class="card">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="card-header">
                                <i class="fa fa-align-justify"></i> Заказчик {{ $user->name }} - ID {{ $user->id }}
                            </div>
                            <div class="card-block">
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="name">НАЗВАНИЕ/ИМЯ</label>
                                    <div class="col-md-9">
                                        <input type="text" id="name" class="form-control" name="name"
                                               value="{{ $user->name }}" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="phone">ТЕЛЕФОН</label>
                                    <div class="col-md-9">
                                        <input type="text" id="phone" class="form-control" name="phone"
                                               value="{{ $user->phone }}" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="email">E-MAIL</label>
                                    <div class="col-md-9">
                                        <input type="text" id="email" class="form-control" name="email"
                                               value="{{ $user->email }}" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="iin">ИИН/БИН</label>
                                    <div class="col-md-9">
                                        <input type="text" id="iin" class="form-control" name="iin"
                                               value="{{ $user->iin }}" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="actual_address">ФИЗИЧЕСКИЙ
                                        АДРЕС</label>
                                    <div class="col-md-9">
                                        <input type="text" id="actual_address" class="form-control"
                                               name="actual_address" value="{{ $user->actual_address }}" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="legal_address">ЮРИДИЧЕСКИЙ
                                        АДРЕС</label>
                                    <div class="col-md-9">
                                        <input type="text" id="legal_address" class="form-control" name="legal_address"
                                               value="{{ $user->legal_address }}" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 form-control-label" for="sphere_id">СФЕРА
                                        ДЕЯТЕЛЬНОСТИ</label>
                                    <div class="col-md-9">
                                        <select class="form-control" id="sphere_id" name="sphere_id">
                                            @foreach($sferes as $sfere)
                                                <option value="{{ $sfere->id }}" @if($sfere->id == $user->sphere_id) selected @endif>{{ $sfere->name_ru }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i>
                                    Сохранить
                                </button>
                                <a href="{{ route('admin.customer.show', ['user' => $user->id]) }}"
                                   class="btn btn-sm btn-danger"><i class="fa fa-ban"></i> Удалить</a>
                            </div>
                        </form>
                    </div>
                    <!--/col-->
                </div>
                <!--/row-->
            </div>
        </div>
        <!-- /.conainer-fluid -->
    </main>

@endsection