@extends('dashboard.layout')
@section('content')

    <main class="main">
        <!-- Breadcrumb -->
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('adminIndex') }}">Панель управления</a></li>
            <li class="breadcrumb-item active">Заказчики</li>
        </ol>
        <div class="container-fluid">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                Заказчики
                            </div>
                            <div class="card-block">

                                <!--/.row-->
                                <br>
                                <table class="table table-hover table-outline mb-0 hidden-sm-down">
                                    <thead class="thead-default">
                                    <tr>
                                        <th>ID</th>
                                        <th class=""><i class="icon-people"></i>
                                        </th>
                                        <th>НАЗВАНИЕ/ИМЯ</th>
                                        <th class="text-center">ТЕЛЕФОН</th>
                                        <th class="text-center">E-MAIL</th>
                                        <th class="text-center">ИИН/БИН</th>
                                        <th class="text-center">ЗАКАЗЫ</th>
                                        <th class="text-center">ДЕЙСТВИЯ</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $user)
                                    <tr>
                                        <td>{{ $user->id }}</td>
                                        <td class="">
                                            <div class="avatar">
                                                @if ($user->avatar)<img src="{{ $user->avatar }}" class="img-avatar"
                                                     alt="admin@bootstrapmaster.com">@endif
                                            </div>
                                        </td>
                                        <td>
                                            <div>{{ $user->name }}</div>
                                            <div class="small text-muted">
                                                Регистрация: {{ $user->created_at }}
                                            </div>
                                        </td>
                                        <td class="text-center">
                                           {{ $user->phone }}
                                        </td>
                                        <td class="text-center">
                                            {{ $user->email }}
                                        </td>
                                        <td class="text-center">
                                            {{ $user->iin }}
                                        </td>
                                        <td class="text-center">
                                            <a href="{{ route('admin.orders.byCustomer', ['customer' => $user->id])}}" class="btn btn-link">
                                                <i class="fa fa-link"></i>&nbsp; Посмотреть заказы ({{ $user->orders->count() }})
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            <div class="input-group flex-center">
                                                <div class="input-group-btn">
                                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Действия
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="{{ route('admin.customer.edit', ['customer' => $user->id]) }}">Изменить</a>
                                                        </li>
                                                        <li><a href="{{ route('admin.customer.show', ['customer' => $user->id]) }}">Удалить</a>
                                                        </li>
                                                    </ul>
                                                </div>

                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/row-->
            </div>
        </div>
        <!-- /.conainer-fluid -->
    </main>

@endsection