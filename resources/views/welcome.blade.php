<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Lukasz Holeczek">
    <meta name="keyword" content="">
    <!-- <link rel="shortcut icon" href="assets/ico/favicon.png"> -->

    <title>MAPPLUS</title>

    <!-- Icons -->
    <link href="/coreui/css/font-awesome.min.css" rel="stylesheet">
    <link href="/coreui/css/simple-line-icons.css" rel="stylesheet">

    <!-- Main styles for this application -->
    <link href="/coreui/css/style.css" rel="stylesheet">

</head>

<body class="app flex-row align-items-center">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-group mb-0">
                <div class="card p-2">
                    <form method="post" action="/admin/account/auth" class="card-block">
                        {{ csrf_field() }}
                        <h1>Вход</h1>
                        <p class="text-muted">Введите код доступа.</p>
                        <div class="input-group mb-2">
                                <span class="input-group-addon"><i class="icon-lock"></i>
                                </span>
                            <input type="password" name="secretKey" class="form-control" placeholder="Password">
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <button class="btn btn-primary px-2">Вход</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card card-inverse card-primary py-3 hidden-md-down" style="width:44%">
                    <div class="card-block text-center">
                        <div>
                            <h2>Вход</h2>
                            <p>Введите код доступа.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap and necessary plugins -->
<script src="/coreui/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/coreui/bower_components/tether/dist/js/tether.min.js"></script>
<script src="/coreui/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>


</body>

</html>
